# 1. Structure of Content:
|Folder|Content Type|Description|
|---|---|---|
|.  |text files|contains README and requirements.txt
|./data  |media and pickle files|contains video/images files used in the competition, and dataframes in pickled format used in training process to access images  
|./data/train_frames   |image files|folder used to contain extracted frames from videos 
|./data/train_videos   |video files|folder used to contain videos of the competition 
|./data/train_faces| image files| folder used to contain extracted faces from training videos
|./data/additional_data   |image files| folder used to contain additional data used for model training
|./models| folders | parent folders for all the model weight files used during the competition  
|./models/good_face_detector|model weight files| model weight file for the good face detector (a.k.a garbage collector) 
|./models/pytorch| model weight files | model weight files for all the models generated from the training process 
|./models/pytorch_original| model weight files | model weight files for all original models used in the competition inference kernels that scored 0.42320 on the private LB
|./models/mobilenet_ssd| model weight files| model weight for SingleShotDetector for face detection
|./model_reproduction|  python scripts and notebook | scripts and notebooks used to perform model training
|./preprocessing|  python scripts and notebook | scripts and notebooks used to generated extracted face dataset, dataframes for extracted face and additional dataset, the good face detector model, and the fake audio list 
|./project_tools|python scripts|utility scripts of the project

<br/><br/>

**Special notes for key files in the *./data/* folder:**
* ./data/img_df.pkl - this dataframe contain meta data of extracted faces from the competition dataset, and need to be regenerated in process of reproduction, please refer to **section 2.1** for more details 
* ./data/additional_df.pkl - this dataframe contain meta data of additional data used in the model training. This contain meta for images from additional dataset that are available acceptable license for the competition. The uploaded version should contain the correct information, but in case of error, please refer to **section 2.1** for more details 
* ./data/flickr-free-files.csv - this file contains all images in the flickr70k dataset that comes with license type acceptable for this competition. 
* ./data/audio_video_labels.csv - this file contain seperate label information for audio and video parts of each video from the competition dataset, code used to generate this file is in the notebook *./preprocessing/audio_metadata_extract.ipynb*

<br/><br/>

# 2. Steps of Reproduction
The essential steps of reproducing the models used in the inference kernels include the following:
1. recreating the face extraction dataset and the pickled dataframe img_df.pkl - see section 2.1
2. rerun model training script for each of the models - see section 2.2

## 2.1 Data Preprocessing
Scripts and example notebooks for creating the face extracted dataset are provided in the **face extraction** folder. In particular, the notebook file end-to-end example of generating the extracted face dataset, and the extracted faces image dataframe are provided in the  Jupyter Notebook file *Face_Extraction_Image_Dataframe_Creation_Example.ipynb*  


<br/>**Frame Extraction**

This is performed by calling the script image_extraction_singlefolder.py to run the frame extraction for each individual folder.

For instance, to extraction frames from videos from the folder dfdc_train_part_0, the run the following in ipython:  
<code>%run image_extraction_singlefolder.py -s '../data/train_videos/dfdc_train_part_0/' -d '../data/train_frames/'</code>

This step need to be repeated on each of 50 parts, and the extracted need to be stored in the same folder. In our soluton, 40 frames were extracted from each video resulted in 2.8TB of data.

Example of using this function is provided in the **Frame Extraction** section of the *Face_Extraction_Image_Dataframe_Creation_Example* notebook


<br/>**Face Extraction** 

This is performed by using the script <code>face_extraction_ssd.py</code>, for example in Ipython:

<code>%run face_extraction_ssd.py</code>

Example of using this function is provided in the  **Face Extraction** section of the *Face_Extraction_Image_Dataframe_Creation_Example* notebook

<br/>**Create face image dataframe**

image dataframe is created by calling the function <code>project_utils.generate_images_files_df_singlefolder()</code>, this needs to work on the original individual video folder structure, where the metadata.json for the corresponding folder is present.

Example of using this function is provided in the **Creation face image dataframe** section of the *Face_Extraction_Image_Dataframe_Creation_Example* notebook

<br/>**Generate height, width, size, height_width_ratio for each extracted face**

Metadata for each extracted face images are calculated, and used in the model training process. The code used to generate these metadata is provided in the **Generate height, width, size, hwratio for each image** section of the *Face_Extraction_Image_Dataframe_Creation_Example* notebook


<br/>**Generate good face ratio for each extracted face**

For some of the latter models, a good face detector were used to probability of a given extracted face of actually being a human face. In this case, a good face could be a human face with or without deepfake manipulation. A face image with a low good face probabily is one that doesn't actually contain the facial area of a person. 

The code used to generate this *good face probability* is provided in the **Generate good face probability** section of the *Face_Extraction_Image_Dataframe_Creation_Example* notebook


<br/>**Creation of dataframe of additional data**
Some of our models are trained with additional images.  An additional dataframe is need to help to manage these images. The code used to generated this additional dataframe is provided in the **Creation of dataframe of additional data** session of the *Face_Extraction_Image_Dataframe_Creation_Example* notebook

During the steps of creating the dataframe for additional data, and the additional images datset itselt, we have taken careful steps to only use images with license that allow commercial usage. Please refer to the *Face_Extraction_Image_Dataframe_Creation_Example* notebook for technical steps. 

<br/>

## 2.2 Model Reproduction
**Important Notes 1:** the labels in our face dataset are created in inversed order: 1 is REAL, and 0 is FAKE.
consequently, our model are trained with this label scheme. We adivse that during model reproduction to keep this inverse order of the labels, to ensure consistency trained model and the inference kernel used to generate the final prediction results. You will see that in post-process step of our kernel that we reverse our prediction according the competition designaed labels - 0 for REAL and 1 FOR FAKE

**Important Notes 2:** all the models are trained with random sampling on the training dataset.  Therefore the reproducued version might generate slighty different predictiosn on test data with evaluation metric at roughly the same ball park as the original version. 

Additionally, dpends on hardwares, one might need to change the <code>batch_size</code> parameter for given model in the file *./project_tools/project_config.py*


<br/>**List of models**

The following table provide a list of models that are used in the competition winning submission. For each model, this include the model name, the original model file, and naming convention for regenerated versions.

In order to provide easy identification, during model reproductions, the name convetion of *model name_efficientnet variant_timestamp_validation_score* is used.  Because the change in timestamp and non-deterministic nature of the model generation process, the regenerated models will have different name compared to the original model. The first part of the file name provides indication which original model a given model is corresponding to. 

|Model Name|Original Model File|Naming Convention for Model Regeneration|
|---|---|---|
hqs01model|cls_b0_256_b128_augv1_newdata_adamW_myhead_model.pth|hqs01model_efnb0_*timestamp*_*validation_score*.pth|
mh3model|mh3_efnb3_202003272155_val0.230851.pth|mh3model_efnb3_*timestamp*_*validation_score*.pth|
st104model|st104_qishen_adaptation_efnb1_202003091249_0.217616_interim.pth|st104model_efnb1_*timestamp*_*validation_score*.pth
st105model|st105_qishen_adaptation_efnb1_202003101316_val0.225449.pth|st105model_efnb1_*timestamp*_*validation_score*.pth
st106model|st106_qishen_adaptation_efnb0_202003072327_val0.287849.pth|st106model_efnb0_*timestamp*_*validation_score*.pth
st111model|st111_qishen_adaptation_efnb4_202003141058_val0.207899.pth|st111model_efnb4_*timestamp*_*validation_score*.pth
st112model|st112_qishen_adaptation_efnb1_202003141812_val0.294983_interim.pth|st112model_efnb1_*timestamp*_*validation_score*.pth
st113model|st113_qishen_adaptation_efnb4_202003150108_val0.269342.pth|st113model_efnb4_*timestamp*_*validation_score*.pth
st119model|st119_qishen_adaptation_efnb0_202003220030_val0.285031.pth|st119model_efnb0_*timestamp*_*validation_score*.pth
st120model|st120_qishen_adaptation_efnb4_202003240013_val0.234563.pth|st120model_efnb4_*timestamp*_*validation_score*.pth
st121model|st121_qishen_adaptation_efnb3_202003260010_val0.153961.pth|st121model_efnb3_*timestamp*_*validation_score*.pth
st123model|st123_qishen_adaptation_efnb0_202003262233_val0.293114.pth|st123model_efnb0_*timestamp*_*validation_score*.pth
st124model|st124_qishen_adaptation_efnb1_202003262238_val0.240297.pth|st124model_efnb1_*timestamp*_*validation_score*.pth
st125model|st125_qishen_adaptation_efnb0_202003271554_val0.31627.pth|st125model_efnb0_*timestamp*_*validation_score*.pth
st126model|st126_qishen_adaptation_efnb1_202003271554_val0.230595.pth|st126model_efnb1_*timestamp*_*validation_score*.pth


<br/>**Reproduction Procedure**

As stated in section 1, all scripts and notebook needed for the reproduction procedure are in provided in the *./model_reproduction/* folder. 

For all all models except *hqs01model*, the python script *models_training_reproduction.py* can be used for the regeneration. This can be triggered by using the following command in commandline:  <code>python models_training_reproduction.py -m MODEL_NAME -g GPU_ID </code> where <code>MODEL_NAME</code> is one of the model names in the above table except *hqs01model*, and <code>GPU_ID</code> is the GPU device to be used in the training process.  For a more step-by-step reproduction procedure, please use the notebook *notebook_models_training_reproduction.ipynb*.

For the model *hqs01model*, please use the notebook *hqs01.ipynb* for the regeneration. A different normalisation stragey were used for image pixel values udring the training process of this model, so it was more straight forward to keep it separately from others models.  This model is also treated differently in the inference kernel. 

<br/><br/>


## 3. Additional Dataset 
The following additional dataset are used in the model training process:
* Flickr 70k real images: we used a [resized version](https://www.kaggle.com/xhlulu/flickrfaceshq-dataset-nvidia-resized-256px) of this dataset. As an additional step, we have used license information available from the [original github](https://github.com/NVlabs/ffhq-dataset) to choose images with license types that is acceptable for this competition.
* Youtube videos images: we manually created a face image dataset from a list of youtube video with licen types that is acceptable for this competition. This list of youtube videos is provided in the notebook *good-face-detector-face-extraction-original.ipynb* in the *good_face_detector* folder. 

<br/><br/>

## 4. Dependency
The python dependency for the work behind this project are provided in the file *requirement.txt* under the root directory of this repository.

Additionally [NVIDIA Apex](https://github.com/NVIDIA/apex) is required to provide mixed-precision computation. 

