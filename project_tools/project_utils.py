import numpy as np
import pandas as pd
import random
from os import listdir
from os.path import isfile, join, isdir
import cv2
import pickle
import sys
import time
from contextlib import contextmanager
from importlib import reload
# import tqdm
from datetime import datetime
from shutil import copyfile, move
import re
from sklearn.preprocessing import binarize
from moviepy.editor import *
from project_tools import project_config, project_utils, project_class, tf_utils, prediction_utils

try:
    from project_tools import torch_utils
except:
    pass

from sklearn.metrics import log_loss, accuracy_score, f1_score
import zipfile
import gc
import glob
from multiprocessing import Pool
from functools import partial
import matplotlib.pyplot as plt
import seaborn as sns


IMAGE_TYPES = 'bmp|jpg|jpeg|png'

# hacky approach to allow quick reload of utility modules during notebook development
def reload_project():
    """
    utility function used during experimentation to reload various model when required, useful for quick experiment iteration
    :return: None
    """
    reload(project_config)
    reload(project_utils)
    reload(project_class)
    reload(tf_utils)
    try:
        reload(torch_utils)
        reload(prediction_utils)
    except:
        pass



@contextmanager
def timer(name):
    """
    utility timer function to check how long a piece of code might take to run.
    :param name: name of the code fragment to be timed
    :yield: time taken for the code to run
    """
    t0 = time.time()
    print('[%s] in progress' % name)
    yield
    print('[%s] done in %.6f s' %(name, time.time() - t0))



def load_data(pickle_file):
    """
    load pickle data from file
    :param pickle_file: path of pickle data
    :return: data stored in pickle file
    """
    load_file = open(pickle_file, 'rb')
    data = pickle.load(load_file)
    return data



def pickle_data(path, data, protocol=-1, timestamp=False, verbose=True):
    """
    Pickle data to specified file
    :param path: full path of file where data will be pickled to
    :param data: data to be pickled
    :param protocol: pickle protocol, -1 indicate to use the latest protocol
    :return: None
    """
    file = path
    if timestamp:
        base_file = os.path.splitext(file)[0]
        time_str = '_' + get_time_string()
        ext = os.path.splitext(os.path.basename(file))[1]
        file = base_file + time_str + ext

    if verbose:
        print('creating file %s' % file)

    save_file = open(file, 'wb')
    pickle.dump(data, save_file, protocol=protocol)
    save_file.close()



def dir_compare(pathl, pathr):
    files_pathl = set([f for f in listdir(pathl) if isfile(join(pathl, f))])
    files_pathr = set([f for f in listdir(pathr) if isfile(join(pathr, f))])
    return list(files_pathl-files_pathr), list(files_pathr-files_pathl)


def lr_dir_sync(pathl, pathr):
    files_lrddiff, files_rldiff = project_utils.dir_compare(pathl, pathr)
    for f in files_lrddiff:
        scr = pathl + f
        dst = pathr + f
        print('copying file %s' % scr)
        copyfile(scr, dst)


def isnotebook():
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False


if isnotebook():
    from tqdm.notebook import tqdm as mtqdm
else:
    from tqdm import tqdm as mtqdm

def check_data_availability(img_df, img_dir=project_config.IMG_DIR):
    for file in img_df['base_file'].values:
        abs_file = img_dir + file
        if not(os.path.isfile(abs_file)):
            print('%s is not found in %s' % (file, img_dir))


def np_corr(a, b):
    return pd.Series(a.flatten()).corr(pd.Series(b.flatten()))

def np_describe(a):
    return pd.Series(a.flatten()).describe()


def np_valuecounts(a):
    return pd.Series(a.flatten()).value_counts()


def np_random_sample(data, size=1):
    idx = np.random.randint(0, len(data), size=size)
    return data[idx]


def get_valuecounts_df(df, feature):
    rename_cols = {'index':feature, feature:feature + '_count'}
    vc_df = df[feature].value_counts().reset_index(drop=False).rename(columns=rename_cols)
    return vc_df

def get_rank(data):
    """ Always useful in AUC competitions """
    ranks = pd.Series(data).rank(pct=True).values
    # return (ranks + 1) / (ranks.max() + 2)
    return ranks


def df_quick_filter_sort(df_vals, df_cols, filter_feature, filter_value, sort_feature, value_feature):
    filter_col_idx = np.argwhere(df_cols == filter_feature)[0][0]
    sort_col_idx = np.argwhere(df_cols == sort_feature)[0][0]
    if sort_feature!=value_feature:
        value_col_idx = np.argwhere(df_cols == value_feature)[0][0]
    row_idx = np.where(df_vals[:, filter_col_idx] == filter_value)
    row_vals = df_vals[row_idx, :][0]
    if sort_feature!=value_feature:
        sort_idx = np.argsort(row_vals[:, sort_col_idx])
        value_vals = row_vals[sort_idx, value_col_idx]
    else:
        value_vals = np.sort(row_vals[:,sort_col_idx])
    return value_vals

def plot_feature_corr(df, features, figsize=(10,10), vmin=0.20):
    val_corr = df[features].corr()
    f, ax = plt.subplots(figsize=figsize)
    sns.heatmap(val_corr, vmin=vmin, square=True)
    return val_corr


def copy_file_with_time(src_file, dst_file_name, des_path=project_config.SCRIPT_BACKUP_PATH):
    basename = os.path.splitext(os.path.basename(dst_file_name))[0]
    ext_name = os.path.splitext(os.path.basename(dst_file_name))[1]
    timestr = get_time_string()
    des_name = '%s%s_%s%s' % (des_path, basename, timestr, ext_name)
    # print(des_name)
    copyfile(src_file, des_name)


def cp_files_with_prefix(src_path, dst_path, prefix, ext=IMAGE_TYPES):
    abs_file_list, base_file_list = get_folder_filelist(src_path, file_type=ext)
#     print(abs_file_list)
    for src_file, base_file in zip(abs_file_list, base_file_list):
        dst_file = dst_path + prefix + base_file
        copyfile(src_file, dst_file)
    return None


def mv_files_with_prefix(src_path, dst_path, prefix, ext=IMAGE_TYPES):
    abs_file_list, base_file_list = get_folder_filelist(src_path, file_type=ext)
#     print(abs_file_list)
    for src_file, base_file in zip(abs_file_list, base_file_list):
        dst_file = dst_path + prefix + base_file
        move(src_file, dst_file)
    return None



def list_intersection(left, right):
    left_set = set(left)
    right_set = set(right)
    return list(left_set.intersection(right_set))

def list_union(left, right):
    left_set = set(left)
    right_set = set(right)
    return list(left_set.union(right_set))




def list_difference(left, right):
    left_set = set(left)
    right_set = set(right)
    return list(left_set.difference(right_set))



def image_numeric_renaming(data_path, ext='.jpg'):
    for smpl in sorted(os.listdir(data_path)):
        img_folder_path = os.path.join(data_path, smpl)
        max_int_len = 1
        max_int = 0
        try:
            for file in os.listdir(img_folder_path):
                print(file)
                if ext in file:
                    # file_int = int(os.path.splitext(file)[0].split('_')[-1])
                    file_int = int(re.findall(r'[0-9]+', file)[0])
                    # print('file_int is %d, max_int is %d' % (file_int, max_int))
                    if file_int>max_int:
                        max_int = file_int
                        max_int_len = len(str(max_int))
                # print('max_len is %d' % max_int_len)
            for file in os.listdir(img_folder_path):
                if ext in file:
                    src_file_name = os.path.join(img_folder_path, file)
                    # file_int = int(os.path.splitext(file)[0].split('_')[-1])
                    file_int = int(re.findall(r'[0-9]+', file)[0])
                    file_str = re.findall(r'[^0-9]+', file)[0]
                    file_int_len = len(str(file_int))
                    file_ext = os.path.splitext(file)[1]
                    if file_int_len<max_int_len:
                        padding_zeros = max_int_len - len(str(file_int))
                        file_int_str = "0" * padding_zeros + str(file_int)
                        # file_name = '_'.join(os.path.splitext(file)[0].split('_')[:-1]) + '_' + file_int_str + file_ext
                        file_name = file_str + file_int_str + file_ext
                        dst_file_name = os.path.join(img_folder_path, file_name)
                        print('renaming %s to %s' % (file, file_name))
                        os.rename(src_file_name, dst_file_name)

        except:
            pass



def zero_padding_int2str(i, numberofzeros=3):
    f = '%0' + str(numberofzeros) + 'd'
    return f % i


def get_time_string():
    now = datetime.now()
    now = str(now.strftime('%Y%m%d%H%M'))
    return now


def image_display(img_array,figsize=(3, 3), cvtcolor=True):
#     cmap_value = 'gray'
    if cvtcolor:
        img_array = cv2.cvtColor(img_array, cv2.COLOR_BGR2RGB)
    plt.figure(figsize=figsize)
    plt.axis("on")
    plt.imshow(np.squeeze(img_array))#, cmap=cmap_value)

    
def check_object_size(obj):
    """
    check the memory size of an object in RAM - used as an utility tool during coding process
    :param obj: an object to be check
    :return: string indicating how bit the object is
    """
    number_of_bytes=sys.getsizeof(obj)
    if number_of_bytes < 0:
        raise ValueError("!!! numberOfBytes can't be smaller than 0 !!!")
    step_to_greater_unit = 1024.
    number_of_bytes = float(number_of_bytes)
    unit = 'bytes'
    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'KB'
    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'MB'
    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'GB'
    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'TB'
    precision = 1
    number_of_bytes = round(number_of_bytes, precision)
    return str(number_of_bytes) + ' ' + unit


def get_folder_filelist(path, file_type=None):
    """
    take the path of a folder as input, output a list of files contained in the input folder
    :param path: file path of the input folder
    :param file_type: file type selector, allow to only output only files with certain extension, default value is 'html'
    :return: a list of file contained in the input folder
    """
    abs_files = []
    base_files = []
    if file_type is not None:
        types = file_type.split('|')
    for f in listdir(path):
        # print(f)
        if isfile(join(path,f)):
            if file_type is None:
                abs_files.append(join(path,f))
            else:
                ext = os.path.splitext(os.path.basename(f))[1][1:]
                if ext in types:
                    abs_files.append(join(path, f))
                    base_files.append(f)

        if isdir(join(path, f)):
            folder_abs_files, folder_base_files = get_folder_filelist(join(path,f), file_type=file_type)
            abs_files += folder_abs_files
            base_files += folder_base_files
    # print(len(abs_files))
    return abs_files, base_files


def glob_folder_filelist(path, file_type='', recursive=False):
    abs_files = []
    base_files = []
    patrn = '**' if recursive else '*'

    if len(file_type) > 0:
        glob_path = path + patrn + '.' + file_type
    else:
        glob_path = path + patrn

    matches = glob.glob(glob_path, recursive=recursive)
    for f in matches:
        if os.path.isfile(f):
            abs_files.append(f)
            base_files.append(os.path.basename(f))
    return abs_files, base_files


def get_folderlist(path):
    folder_list = []
    for f in listdir(path):
        folder_path = join(path, f) + '/'
        if isdir(folder_path):
            folder_list.append(folder_path)
    return folder_list

def get_part_str(input_str, sep='_'):
    if (input_str is None) or (len(input_str)==0):
        return ''
    else:
        return input_str + sep



def pd_split_byfeature(df, split_feature, train_ratio=0.75):
    original_group_series = pd.Series(df[split_feature].unique())
    train_length = int(len(original_group_series) * train_ratio)
    train_original_groups = original_group_series.sample(train_length).tolist()
    valid_original_groups = project_utils.list_difference(original_group_series.tolist(), train_original_groups)

    val_idx = df.loc[df[split_feature].isin(valid_original_groups)].index.values
    train_idx = df.iloc[df.index.difference(val_idx)].index.values
    ltrain_df = df.iloc[train_idx].copy().reset_index(drop=True)
    lval_df = df.iloc[val_idx].copy().reset_index(drop=True)

    return ltrain_df, lval_df

####  Image related functions

def get_single_image_array(file, grayscale=False, figsize=(3, 3), display=True, cvtcolor=False):
    """
    get the data array for a image by providing the full path of the image, and return the image array
    :param file: full path of the image
    :param grayscale: if True, display grayscale image; if False, display color image
    :param figsize: tuple that indicates the size of the figure area
    :param display: if True, display the image; if False, no display is triggered
    :return: cimg: the array representing the image
    """
    cmap_value = None
    img = cv2.imread(file)
    if grayscale:
        cimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    elif cvtcolor:
        cimg = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    else:
        cimg = img
    if grayscale: cmap_value = 'gray'
    if display:
        plt.figure(figsize=figsize)
        plt.axis("on")
        plt.imshow(cimg, cmap=cmap_value)
    return cimg


def generate_image_array(image_list, size=(96,96), grayscale=False, verbose=False, cvtcolor=False, use_imageio=False, smart_resize=True):
    """
    generate an array with each element representing one image.
    :param image_list: a list of file names, with each file name pointing to one image
    :param size: tuple that decides the resize factor
    :param grayscale: if True, generate image array for grayscale images; if False, generate image array for color image
    :param verbose: if True, display which image is being processed; if False, no progress information is displayed
    :return: an array with each element representing one image.
    """
    img_rs_array = []
    image_type = 'grayscale' if grayscale else 'color'
    if verbose:
        print("generating {} image array".format(image_type))
    for img_file in image_list:
        if verbose:   print('resizing image {}'.format(img_file))
        temp_img = get_single_image_array(img_file, grayscale=grayscale, display=False, cvtcolor=cvtcolor)
        if smart_resize:
            temp_img = isotropically_resize_image(temp_img, size[0])
            temp_img = make_square_image(temp_img)
        else:
            temp_img = cv2.resize(temp_img, dsize=size)

        img_rs_array.append(temp_img)
    return np.array(img_rs_array)


def random_display(df, n=10):
    for i in df.sample(n).index:
        project_utils.get_single_image_array(df['abs_file'].values[i], display=True, cvtcolor=True)





def array2img_set_display(arr, nrow=2, ncol=5, random=False, grayscale=True, figsize=(30,15), labels=[]):
    """
    display a set of images from the input list of image arrays
    :param images: dataframe containing key meta data for the images
    :param nrow: number of rows of images
    :param ncol: number of columns of images
    :param random: if True, to display random set of images; if False, to display the first nrow * ncol of images
    :param grayscale: if True, to display grayscale images; if False, to display color images
    :param figsize: tuple that indicates the size of the figure area
    :return: None
    """
    fig=plt.figure(figsize=figsize)
    cmap_value=None
    for i in range(nrow * ncol):
        if random:
            index = np.random.randint(0, len(arr))
        else:
            index = i
        img = arr[index]
        ax = fig.add_subplot(nrow, ncol, i + 1, xticks=[], yticks=[])
        if grayscale: cmap_value='gray'
        ax.imshow(img, cmap=cmap_value)

        if len(labels)>0:
            if labels[index] == 1:
                label = 'positive'
            else:
                label = 'negative'
            ax.set_title("ID {} - {}".format(index, label), fontsize=20)
        else:
            ax.set_title("ID {}".format(index), fontsize=20)



def unzip_folder_file(src, dst):
    zip_files, base_file = get_folder_filelist(src, file_type='zip')
    mtq = mtqdm(zip_files)
    for file in mtq:
        with zipfile.ZipFile(file, 'r') as zip_ref:
            zip_ref.extractall(dst)



def folder_image_resize(src, dst, dim=350):
    img_files = get_folder_filelist(src, file_type='jpg')[0]
    mtq = mtqdm(img_files)
    if not(os.path.isdir(dst)):
        os.mkdir(dst)

    for file in mtq:
        base_name = os.path.basename(file)
        img_array = get_single_image_array(file, display=False)
        img_rs_array = cv2.resize(img_array, dsize=(dim, dim))
        dst_name = dst + base_name
        cv2.imwrite(dst_name, img_rs_array)





def single_image_resize(file, dst, dim=350):
    base_name = os.path.basename(file)
    img_array = get_single_image_array(file, display=False)
    img_rs_array = cv2.resize(img_array, dsize=(dim, dim))
    dst_name = dst + base_name
    cv2.imwrite(dst_name, img_rs_array)




def folder_image_resize_mp(src, dst, dim=350, file_type = 'jpg', njobs=2):
    files = glob_folder_filelist(src, file_type=file_type)[0]
    if not(os.path.isdir(dst)):
        os.mkdir(dst)

    p = Pool(njobs)
    # with Pool(njobs) as p:
    with p:
        res = p.map(partial(single_image_resize, dst=dst, dim=dim), files)

    p.close()
    return res




def prediction_results_interpretation(true, preds, pos_class=1):
    """
    take a set of prediction result, and calculate the f1 score, precision, recall and accuracy
    :param true: labels of the data
    :param preds: a set of predictions
    :param pos_class: in a binary classification scenario, the class which f1 score, precision, recall and accuracy to be calculated
    :return: f1 score, precision recall and accuracy of the input prediction with regarding to the label
    """
    pos_value=pos_class
    neg_value=int(not(bool(pos_class)))
    tp = ((true == pos_value) & (preds == true)).sum()
    fp = ((true == neg_value) & (preds != true)).sum()
    fn = ((true == pos_value) & (preds != true)).sum()
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = 2 * precision * recall / (precision + recall)
    accuracy = (true == preds).sum() / len(true)
    return f1, precision, recall, accuracy


def pretty_print_results(true, preds):
    """
    print the prediction metric in a presentable format
    :param true: labels of the data
    :param preds: a set of predictions
    :return: None
    """
    positive_output = prediction_results_interpretation(true, preds, pos_class=1)
    print('positive_output class - F1 score is {:.5f}'.format(positive_output[0]))
    print('positive_output class - precision score is {:.5f}'.format(positive_output[1]))
    print('positive_output class - recall score is {:.5f}'.format(positive_output[2]))
    print('Accuracy is {:.5f}'.format(positive_output[3]))





def print_threshold_graph(y_train, y_pred, sample_class='positive'):
    """
    Plot a graph to show how different cut-off percentage for binary classification would generate metric score
    :param y_pred: prediction in probability, direct output from the keras model
    :param y_train: labels for the training data points
    :param sample_class: if 'incorrect', display metrics for the incorrect class; if 'correct', display metrics for the correct class;
                         if 'both', display metrics for both classes
    :return: None
    """

    negative_f1 = []
    negative_precision = []
    negative_recall = []
    positive_f1 = []
    positive_precision = []
    positive_recall = []
    accuracy = []

    # list of percentage cut-off points to be displayed
    pers = [0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75,
            0.80, 0.85, 0.90, 0.95]

    negative_items = ['negative_f1', 'negative_precision', 'negative_recall']
    positive_items = ['positive_f1', 'positive_precision', 'positive_recall']

    for per in pers:
        pred = binarize(y_pred, threshold=per).flatten().astype(np.uint8)
        negative_f1.append(prediction_results_interpretation(y_train, pred, pos_class=0)[0])
        negative_precision.append(prediction_results_interpretation(y_train, pred, pos_class=0)[1])
        negative_recall.append(prediction_results_interpretation(y_train, pred, pos_class=0)[2])
        accuracy.append(prediction_results_interpretation(y_train, pred, pos_class=0)[3])
        positive_f1.append(prediction_results_interpretation(y_train, pred, pos_class=1)[0])
        positive_precision.append(prediction_results_interpretation(y_train, pred, pos_class=1)[1])
        positive_recall.append(prediction_results_interpretation(y_train, pred, pos_class=1)[2])

    cols = negative_items + positive_items + ['accuracy']
    if sample_class == 'negative':
        negative_items.append('accuracy')
        items = negative_items

    if sample_class == 'positive':
        positive_items.append('accuracy')
        items = positive_items

    if sample_class == 'both':
        items = negative_items + positive_items + ['accuracy']

    combine = np.vstack([negative_f1, negative_precision, negative_recall,
                         positive_f1, positive_precision, positive_recall, accuracy])

    df = pd.DataFrame(columns=pers, index=cols, data=combine)
    df = df.transpose()
    df.reset_index(drop=False, inplace=True)
    df.rename(columns={'index': 'threshold'}, inplace=True)
    df.plot(x="threshold", y=items, kind="line", figsize=(20, 10))




def prediction2img_display(arr, labels, preds,  nrow=3, ncol=4, random=False, figsize=(30,20), fontsize=20):
    """
    display a set of images from the input image arrays, and display the prediction and label for each displayed image
    :param arr: an list of image arrays - each array contain information for one image
    :param preds: prediction from the model - each value corresponds to class probability of one image
    :param labels: ground truth of the target
    :param nrow: number of rows of images to be displayed
    :param ncol: number of columns of images to be displayed
    :param random: if True, display a random list of images; if False, display the first nrow*ncol images
    :param figsize: tuple that indicates the size of the figure area
    :return: None
    """
        
    fig=plt.figure(figsize=figsize)
    if len(np.array(arr.shape))==4:
        cmap_value = None
    else:
        cmap_value = 'gray'
    for i in range(nrow * ncol):
        if random:
            index = np.random.randint(0, len(arr))
        else:
            index = i
        img = arr[index]
        ax = fig.add_subplot(nrow, ncol, i + 1, xticks=[], yticks=[])
        ax.imshow(img, cmap=cmap_value)

        if len(labels)>0:
            # if labels are provided
            label = 'positive' if labels[index]==1 else 'negative'
            prediction = 'positive' if preds[index]==1 else 'negative'
            color = 'green' if label==prediction else 'red'
            ax.set_title("ID:{} - label:{} - prediction:{} ".format(index, label, prediction), fontsize=fontsize, color=color)
        else:
            # if labels are not provided
            prediction = 'positive' if preds[index]==1 else 'negative'
            color = 'blue'
            ax.set_title("ID:{} - prediction:{} ".format(index, prediction), fontsize=fontsize, color=color)
            
            

            
            
def predprob2img_display(arr, labels, preds, threshold=0.5, nrow=3, ncol=4, random=False, figsize=(30,20), fontsize=20):
    """
    display a set of images from the input image arrays, and display the prediction and label for each displayed image
    :param arr: an list of image arrays - each array contain information for one image
    :param preds: prediction from the model - each value corresponds to class probability of one image
    :param labels: ground truth of the target
    :param nrow: number of rows of images to be displayed
    :param ncol: number of columns of images to be displayed
    :param random: if True, display a random list of images; if False, display the first nrow*ncol images
    :param figsize: tuple that indicates the size of the figure area
    :return: None
    """
        
    fig=plt.figure(figsize=figsize)
    if len(np.array(arr.shape))==4:
        cmap_value = None
    else:
        cmap_value = 'gray'
    for i in range(nrow * ncol):
        if random:
            index = np.random.randint(0, len(arr))
        else:
            index = i
        img = arr[index]
        ax = fig.add_subplot(nrow, ncol, i + 1, xticks=[], yticks=[])
        ax.imshow(img, cmap=cmap_value)

        prediction = preds[index]
        color = 'blue'
        if len(labels)>0:
            # if labels are provided
            label = 'positive' if labels[index] == 1 else 'negative'
            prediction_str = '%0.4f' % prediction
            prediction_bin = 'positive' if preds[index] >= threshold else 'negative'
            color = 'green' if label == prediction_bin else 'red'
            ax.set_title("ID:{} - label:{} - prediction:{} {}".format(index, label, prediction_bin, prediction_str), fontsize=fontsize, color=color)

            # ax.set_title("ID:{} - label:{} - prediction:{}".format(index, label, prediction_str), fontsize=fontsize, color=color)
        else:
            # if labels are not provided
            prediction_str = '%0.4f' % prediction
            ax.set_title("ID:{} - prediction:{}".format(index, prediction_str), fontsize=fontsize, color=color)




def capture_images(video_file, image_folder, n_parts=None, time_step=10, duration=None, verbose=False, color_convert=True, group_name=None, label=None):
    image_basename = os.path.splitext(os.path.basename(video_file))[0]
    video = VideoFileClip(video_file)
    if duration is None: duration = video.duration
    if n_parts is not None: time_step = duration/n_parts

    max_intlen = len(str(round(int(duration/time_step)*time_step, 5)))

    label_str = get_part_str(label)
    group_str = get_part_str(group_name)
    image_base_name_str = get_part_str(image_basename)

    res = []
    for i in range(int(duration/time_step)):
        t =  i * time_step
        if t > duration: break
        # print(t)
        time_str = '{0:.2f}'.format(round(t,5))
        padding_zeros = max_intlen - len(time_str)

        file_int_str = "0" * padding_zeros + time_str
        # print(max_intlen,round(t,5), padding_zeros, file_int_str)
        image_name = image_folder + label_str + group_str + image_base_name_str + file_int_str + '.jpg'
        video_frame = video.get_frame(t)
        if verbose:
            print("saving image %s" % image_name)
        # scipy.misc.imsave(image_name, video_frame)
        if color_convert:
            cv2.imwrite(image_name, cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB))
        else:
            cv2.imwrite(image_name, video_frame)
        res.append(image_name)

    del video
    gc.collect()
    return res


def capture_images_cv_byframe(video_file, image_folder, n_parts=40, color_convert=False, group_name=None, label=None,verbose=False):
    image_basename = os.path.splitext(os.path.basename(video_file))[0]
    cap = cv2.VideoCapture(video_file)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_ids = np.uint16(np.linspace(0, frame_count, num=n_parts))
    max_intlen = len(str(frame_count))
    label_str = get_part_str(label)
    group_str = get_part_str(group_name)
    image_base_name_str = get_part_str(image_basename)
    res = []
    for frame_id in frame_ids:
        padding_zeros = max_intlen - len(str(frame_id))
        file_int_str = "0" * padding_zeros + str(frame_id)
        image_name = image_folder + label_str + group_str + image_base_name_str + file_int_str + '.jpg'
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame_id)
        ret, video_frame = cap.read()
        if verbose:
            print("saving image %s" % image_name)
        if color_convert:
            cv2.imwrite(image_name, cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB))
        else:
            cv2.imwrite(image_name, video_frame)
        res.append(video_frame)
    cap.release()
    return res


def capture_images_cv_stream(video_file, image_folder, n_parts=40, color_convert=False, group_name=None, label=None,verbose=False):
    image_basename = os.path.splitext(os.path.basename(video_file))[0]
    cap = cv2.VideoCapture(video_file)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_ids = np.uint16(np.linspace(0, frame_count-1, num=n_parts))
    max_intlen = len(str(frame_count))
    label_str = get_part_str(label)
    group_str = get_part_str(group_name)
    image_base_name_str = get_part_str(image_basename)
    res = []
    for frame_index in range(int(frame_count)):
        ret = cap.grab()
        if frame_index in frame_ids:
            ret, video_frame = cap.retrieve()
            padding_zeros = max_intlen - len(str(frame_index))
            file_int_str = "0" * padding_zeros + str(frame_index)
            image_name = image_folder + label_str + group_str + image_base_name_str + file_int_str + '.jpg'
            if verbose:
                print("saving image %s" % image_name)
            if color_convert:
                cv2.imwrite(image_name, cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB))
            else:
                cv2.imwrite(image_name, video_frame)
            res.append(image_name)
    cap.release()
    return res


def video2images(video_file, n_parts=40, color_convert=False):
    cap = cv2.VideoCapture(video_file)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_ids = np.uint16(np.linspace(0, frame_count-1, num=n_parts))
    res = []
    for frame_index in range(int(frame_count)):
        ret = cap.grab()
        if frame_index in frame_ids:
            ret, video_frame = cap.retrieve()
            if color_convert:
                video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB)
            res.append(video_frame)
    cap.release()
    return res



def get_df_from_json(json_file):
    df = pd.read_json(json_file).T
    df.reset_index(drop=False, inplace=True)
    rename_col = {'index': 'video'}
    df.rename(columns=rename_col, inplace=True)
    return df


def extract_images_from_videos(videos_path, image_parent_folder, n_parts=None, time_step=10, use_cv=True):
    video_files = get_folder_filelist(videos_path,file_type='mp4')[0]
    video_df = get_df_from_json(videos_path+'metadata.json')
    group_folder_name = os.path.basename(os.path.dirname(videos_path))
    if group_folder_name in project_config.TRAIN_GROUP_MAP.keys():
        group_name = project_config.TRAIN_GROUP_MAP[group_folder_name]
    else:
        group_name = None

    mtq = mtqdm(video_files)
    video_images = []
    for video_file in mtq:
        print(video_file)
        label = video_df[video_df['video']==os.path.basename(video_file)]['label'].values[0]
        # print(video_file, label)
        # image_folder = image_parent_folder + os.path.splitext(os.path.basename(video_file))[0]+'/'
        if not os.path.exists(image_parent_folder):
            os.makedirs(image_parent_folder)
        if use_cv:
            image_list = capture_images_cv_stream(video_file, image_parent_folder, n_parts=n_parts, group_name= group_name, label= label)
        else:
            image_list = capture_images(video_file, image_parent_folder, n_parts=n_parts, time_step=time_step, group_name= group_name, label= label)
        video_images += image_list
        gc.collect()
    return video_images


def extract_images_from_subfolder_videos(videos_path, image_parent_folder, n_parts=None, time_step=10):
    folder_list = get_folderlist(videos_path)
    all_images = []
    for folder in folder_list:
        print(folder)
        folder_image_list = extract_images_from_videos(folder, image_parent_folder, n_parts, time_step)
        all_images += folder_image_list
    return all_images


def show_frame_from_videos(video_file, t=10, color_convert=False):
    video = VideoFileClip(video_file)
    if t<=video.duration:
        frame = video.get_frame(t)
        if color_convert:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        project_utils.image_display(frame)
    else:
        print('video is shorter than the specified t value')



def df_colorcvt(df, des_folder, cvt_groups=[]):
    for index, row in df.iterrows():
        file_path = row['abs_file']
        file_name = row['base_file']
        new_file_path = des_folder + file_name
        group = row['group']
        if group in cvt_groups:
            print('coverting file %s' % file_name)
            img = project_utils.get_single_image_array(file_path, display=False)
            cimg = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            cv2.imwrite(new_file_path, cimg)
        else:
            copyfile(file_path, new_file_path)


def setup_labelled_imgs(imgstrs, dst_path, prefix='', org_dir=project_config.IMG_DIR):
    labels = ['pos', 'neg']
    print('destination is %s' % dst_path)
    for imgs in imgstrs:
        for label in labels:
            src_path = org_dir + imgs + '_' + label
            print('copying from %s' % src_path)
            try:
                cp_files_with_prefix(src_path, dst_path, prefix=imgs+'_')
            except:
                pass


# generate binary predication with simple 0.5 threshold
def prediction_evaluation(val_df, y_pred, threshold):
    output_dict = {}
    print('threshold = %0.2f' % threshold)
    output_dict['threashold'] = threshold

    y_valid = val_df['label'].values
    y_pred_bin = binarize(y_pred, threshold=threshold).flatten().astype(np.uint8)
    # check logloss score
    lloss = log_loss(val_df['label'], y_pred, eps=1e-7)
    # check binary prediction accuracy
    accuracy = accuracy_score(y_valid, y_pred_bin)
    # check f1_score
    f1 = f1_score(val_df['label'], y_pred_bin)
    print('logloss score is %0.6f' % lloss)
    print('overall accuracy is %0.6f' % accuracy)
    print('overall f1 score is %0.6f' % f1)
    output_dict['overall logloss'] = lloss
    output_dict['overall accuracy'] = accuracy
    output_dict['overall f1 score'] = f1

    vid_neg_idx = val_df.loc[val_df['label'] == 0].index.values
    y_neg_valid = y_valid[vid_neg_idx]
    y_neg_pred = y_pred[vid_neg_idx]
    y_neg_pred_bin = binarize(y_neg_pred, threshold=threshold).flatten().astype(np.uint8)
    y_pred_neg_accuracy = accuracy_score(y_neg_valid, y_neg_pred_bin)
    print('valid neg accuracy is %0.6f' % y_pred_neg_accuracy)
    output_dict['negative accuracy'] = y_pred_neg_accuracy

    vid_pos_idx = val_df.loc[val_df['label'] == 1].index.values
    y_pos_valid = y_valid[vid_pos_idx]
    y_pos_pred = y_pred[vid_pos_idx]
    y_pos_pred_bin = binarize(y_pos_pred, threshold=threshold).flatten().astype(np.uint8)
    y_pred_pos_accuracy = accuracy_score(y_pos_valid, y_pos_pred_bin)
    print('valid pos accuracy is %0.6f' % y_pred_pos_accuracy)
    output_dict['positive accuracy'] = y_pred_pos_accuracy

    avg_accuracy = (y_pred_pos_accuracy + y_pred_neg_accuracy)/2
    print('mean pos neg accuracy is %0.4f' % avg_accuracy)
    output_dict['mean accuracy'] = avg_accuracy
    return output_dict










def model_dfgen_predict(df, model, batch_size=256):
    df_len = len(df)
    dim = model.layers[0].input_shape[1]
    dfgen = project_class.DataFrame_Generator(df, dim=dim, aug_func=None, shuffle=False)
    steps = (df_len//batch_size)+1
    y_pred = model.predict_generator(dfgen.flow(batch_size=batch_size), steps=steps, verbose=1)[0:df_len]
    return y_pred



def get_maxerr_df(train_df, model, threshold=0.50):
    y_pred = model_dfgen_predict(train_df, model)
    train_df['abs_err'] = np.abs(train_df['label'] - y_pred.flatten())
    res_df = train_df[train_df['abs_err'] > threshold].reset_index(drop=True)
    return res_df



# project specific function
def generate_images_files_df_singlefolder(image_path, file_type='jpg', recursive=False):
    image_df = pd.DataFrame()
    # img_list, base_img_list = get_folder_filelist(image_path, file_type='jpg')
    img_list, base_img_list = glob_folder_filelist(image_path, file_type=file_type, recursive=recursive)
    labels = []
    groups = []
    # timestamps = []
    videos = []
    for img in base_img_list:
        items = img.split('_')
        if 'REAL' in img:
            labels.append(1)
        else:
            labels.append(0)

        groups.append(items[1])
        # timestamps.append(items[3][:-4])
        videos.append(items[2]+'.mp4')
    image_ids = np.arange(len(img_list))
    image_df['id'] = image_ids
    image_df['abs_file'] = img_list
    image_df['base_file'] = base_img_list
    image_df['group'] = groups
    image_df['video'] = videos
    # image_df['timestamp'] = timestamps
    image_df['label'] = labels
    return image_df



def plot_prediction_result(result_df, label='y_valid', features = ['y_pred_max', 'y_pred_mean'], bins=20):
    print(result_df[label].value_counts())
    for feature in features:
        for i, d in result_df.groupby(label):
            d[feature].plot(kind='hist', figsize=(15, 5), bins=bins, alpha=0.8, title=feature)
            plt.legend(['FAKE','REAL'])
        plt.show()
        print(result_df[feature].describe())


def sample_from_group(g, n_frames=5, tf_ratio=2):
    g_true_images = g[g['label'] == 1]['base_file'].sample(n_frames).tolist()
    g_fake_images = g[g['label'] == 0]['base_file'].sample(n_frames * tf_ratio).tolist()
    return g_true_images, g_fake_images


def df_balance_sampling(df, class_feature, minor_class=1, sample_ratio=1):
    minor_df = df[df[class_feature]==minor_class]
    major_df = df[df[class_feature]==(1-minor_class)].sample(sample_ratio * len(minor_df))

    res_df = minor_df.append(major_df)
    res_df = res_df.sample(len(res_df)).reset_index(drop=True)
    return res_df



def get_sample_image_lists(img_df, n_frames=5, tf_ratio=2, video_samples=project_config.TOTAL_ORIGINAL):
    count =0
    img_df = img_df.sample(frac=1).reset_index(drop=True)
    groups = img_df.groupby('original')
    true_images = []
    fake_images = []
    mg = project_utils.mtqdm(groups)
    for i, g in mg:
        g_true_images, g_fake_images = sample_from_group(g, n_frames=n_frames, tf_ratio=tf_ratio)
        true_images += g_true_images
        fake_images += g_fake_images
        count += 1
        if count > video_samples:
            break
    return true_images, fake_images


def get_sample_image_lists_per_video(img_df, n_frames=1, video_samples=project_config.TOTAL_ORIGINAL):
    count =0
    img_df = img_df.sample(frac=1).reset_index(drop=True)
    groups = img_df.groupby('original')

    true_images = []
    fake_images = []
    mg = project_utils.mtqdm(groups)
    for i, g in mg:
        g_true_video = g[g['label']==1]['video'].tolist()[0]
        g_fake_video = g[g['label']==0]['video'].sample(1).tolist()[0]

        g_true_images = g[g['video']==g_true_video]['abs_file'].sample(n_frames).tolist()
        g_fake_images = g[g['video']==g_fake_video]['abs_file'].sample(n_frames).tolist()

        true_images += g_true_images
        fake_images += g_fake_images
        count += 1
        if count > video_samples:
            break
    return true_images, fake_images




def video2faces_sdd(video_file, n_parts=10, color_convert=False, enlarge_ratio=1):
    faces_in_video = []
    sdd_dector = project_class.TensoflowMobilNetSSDFaceDector()
    cap = cv2.VideoCapture(video_file)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    if n_parts != 1:
        frame_ids = np.uint16(np.linspace(0, frame_count - 1, num=n_parts))
    else:
        frame_ids = [int(frame_count/2)]
    for frame_index in range(int(frame_count)):
        ret = cap.grab()
        if frame_index in frame_ids:
            ret, video_frame = cap.retrieve()
            if color_convert:
                video_frame = cv2.cvtColor(image_np=video_frame)

            faces = sdd_dector.detect_face(video_frame)
            if len(faces) > 0:
                frame_faces = []
                for face in faces:
                    x1 = face[0]
                    y1 = face[1]
                    x2 =  face[2]
                    y2 =  face[3]

                    if enlarge_ratio!=1:
                        org_x1 = x1
                        org_y1 = y1
                        org_w = x2 - x1
                        org_h = y2 - y1
                        cx = org_x1 + int(org_w / 2)
                        cy = org_y1 + int(org_h / 2)
                        max_x = video_frame.shape[1] - 1
                        max_y = video_frame.shape[0] - 1
                        nw = int(org_w * enlarge_ratio)
                        nh = int(org_h * enlarge_ratio)
                        x1 = cx - int(nw / 2)
                        y1 = cy - int(nh / 2)
                        x2 = x1 + nw
                        y2 = y1 + nh
                        x1 = x1 if x1 >= 0 else 0
                        x2 = x2 if x1 <= max_x else max_x
                        y1 = y1 if y1 >= 0 else 0
                        y2 = y2 if y2 <= max_y else max_y

                    crop_img = video_frame[y1:y2, x1:x2, :]
                    frame_faces.append(crop_img)
                faces_in_video += frame_faces
    cap.release()
    return faces_in_video






#
#
# def video2faces_mtcnn(video_file, n_parts=10, color_convert=False):
#     faces_in_video = []
#     detector = MTCNN()
#     cap = cv2.VideoCapture(video_file)
#     frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
#     if n_parts != 1:
#         frame_ids = np.uint16(np.linspace(0, frame_count - 1, num=n_parts))
#     else:
#         frame_ids = [int(frame_count/2)]
#     for frame_index in range(int(frame_count)):
#         ret = cap.grab()
#         if frame_index in frame_ids:
#             ret, video_frame = cap.retrieve()
#             if color_convert:
#                 video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB)
#
#             faces = detector.detect_faces(video_frame)
#             if len(faces) > 0:
#                 frame_faces = []
#                 for face in faces:
#                     face_box = face['box']
#                     x1 = face_box[0]
#                     y1 = face_box[1]
#                     w = face_box[2]
#                     h = face_box[3]
#                     crop_img = video_frame[y1:y1+h, x1:x1+w, :]
#                     frame_faces.append(crop_img)
#                 faces_in_video += frame_faces
#     cap.release()
#     return faces_in_video







def generate_meta_features(meta_file, val_preds_folder):
    baseline_meta = project_utils.load_data(meta_file)
    val_pred_files = project_utils.glob_folder_filelist(val_preds_folder)[0]
    if 'lvalid' in baseline_meta.keys():
        lval_df = baseline_meta['lvalid']
    else:
        lval_df = baseline_meta['lvalid_df']

    cols = ['base_file', 'group', 'video', 'original', 'label']
    model_names = []
    for file in val_pred_files:
        lval_pred = project_utils.load_data(file)
        lloss = log_loss(lval_df['label'], lval_pred, eps=1e-7)
        model_name = os.path.basename(file)[0:-4]
        model_names.append(model_name)
        lval_df[model_name] = lval_pred
        cols.append(model_name)
    return lval_df[cols], model_names






def video2faces(video_file, face_detector, n_parts=10, color_convert=False, enlarge_ratio=1.3):
    faces_in_video = []
    cap = cv2.VideoCapture(video_file)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_ids = np.uint16(np.linspace(0, frame_count - 1, num=n_parts))
    for frame_index in range(int(frame_count)):
        ret = cap.grab()
        if frame_index in frame_ids:
            ret, video_frame = cap.retrieve()
            if color_convert:
                video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB)

            faces = face_detector(video_frame)
            if len(faces) > 0:
                frame_faces = []
                for face in faces:
                    org_x1 = face.left()
                    org_y1 = face.top()
                    org_w = face.right() - org_x1
                    org_h = face.bottom() - org_y1
                    cx = org_x1 + int(org_w / 2)
                    cy = org_y1 + int(org_h / 2)
                    max_x = video_frame.shape[1] - 1
                    max_y = video_frame.shape[0] - 1
                    nw = int(org_w * enlarge_ratio)
                    nh = int(org_h * enlarge_ratio)
                    x1 = cx - int(nw / 2)
                    y1 = cy - int(nh / 2)
                    x2 = x1 + nw
                    y2 = y1 + nh
                    x1 = x1 if x1 >= 0 else 0
                    x2 = x2 if x1 <= max_x else max_x
                    y1 = y1 if y1 >= 0 else 0
                    y2 = y2 if y2 <= max_y else max_y
                    crop_img = video_frame[y1:y2, x1:x2, :]
                    frame_faces.append(crop_img)
                faces_in_video += frame_faces
    cap.release()
    return faces_in_video




def dlib_facedetector(image_file, face_detector, dst_folder, enlarge_ratio=1.3):
    basename = os.path.basename(image_file)[:-4]
    res = get_single_image_array(image_file, cvtcolor=False, display=False)
    faces = face_detector(res)
    res_files = []
    if len(faces) > 0:
        face_id = 1
        for face in faces:
            org_x1 = face.left()
            org_y1 = face.top()
            org_w = face.right() - org_x1
            org_h = face.bottom() - org_y1
            cx = org_x1 + int(org_w / 2)
            cy = org_y1 + int(org_h / 2)
            max_x = res.shape[1] - 1
            max_y = res.shape[0] - 1
            nw = int(org_w * enlarge_ratio)
            nh = int(org_h * enlarge_ratio)
            x1 = cx - int(nw / 2)
            y1 = cy - int(nh / 2)
            x2 = x1 + nw
            y2 = y1 + nh
            x1 = x1 if x1 >= 0 else 0
            x2 = x2 if x1 <= max_x else max_x
            y1 = y1 if y1 >= 0 else 0
            y2 = y2 if y2 <= max_y else max_y
            crop_img = res[y1:y2, x1:x2, :]
            face_str = 'f' + str(len(faces)) + 'id' + str(face_id)
            dst_file = dst_folder + basename + '_' + face_str + '.jpg'
            cv2.imwrite(dst_file, crop_img)
            res_files.append(dst_file)
            face_id += 1
        if len(res_files)>1:
            return ','.join(res_files)
        else:
            return dst_file
    else:
        return ''


def dlib_facedetector_mp(image_lists, face_detector, dst_folder, enlarge_ratio=1.3, njobs=2):
    # files = glob_folder_filelist(src, file_type=file_type)[0]
    if not(os.path.isdir(dst_folder)):
        os.mkdir(dst_folder)

    p = Pool(njobs)
    # with Pool(njobs) as p:
    with p:
        res = p.map(partial(dlib_facedetector, face_detector=face_detector, dst_folder=dst_folder, enlarge_ratio=enlarge_ratio), image_lists)
    p.close()
    return res




def ssd_facedetector(image_file, face_detector, dst_folder, enlarge_ratio=1.3):
    if not(os.path.isdir(dst_folder)):
        os.mkdir(dst_folder)

    basename = os.path.basename(image_file)[:-4]
    image = get_single_image_array(image_file, cvtcolor=False, display=False)
    faces = face_detector.detect_face(image, from_file=False)
    res_files = []
    if len(faces) > 0:
        face_id = 1
        for face in faces:
            x1 = face[0]
            y1 = face[1]
            x2 = face[2]
            y2 = face[3]

            if enlarge_ratio != 1:
                org_x1 = x1
                org_y1 = y1
                org_w = x2 - x1
                org_h = y2 - y1
                cx = org_x1 + int(org_w / 2)
                cy = org_y1 + int(org_h / 2)
                max_x = image.shape[1] - 1
                max_y = image.shape[0] - 1
                nw = int(org_w * enlarge_ratio)
                nh = int(org_h * enlarge_ratio)
                x1 = cx - int(nw / 2)
                y1 = cy - int(nh / 2)
                x2 = x1 + nw
                y2 = y1 + nh
                x1 = x1 if x1 >= 0 else 0
                x2 = x2 if x1 <= max_x else max_x
                y1 = y1 if y1 >= 0 else 0
                y2 = y2 if y2 <= max_y else max_y
            crop_img = image[y1:y2, x1:x2, :]

            face_str = 'f' + str(len(faces)) + 'id' + str(face_id)
            dst_file = dst_folder + basename + '_' + face_str + '.jpg'
            cv2.imwrite(dst_file, crop_img)
            res_files.append(dst_file)
            face_id += 1
        if len(res_files)>1:
            return ','.join(res_files)
        else:
            return dst_file
    else:
        return ''







def prewhiten(x):
    if x.ndim == 4:
        axis = (1, 2, 3)
        size = x[0].size
    elif x.ndim == 3:
        axis = (0, 1, 2)
        size = x.size
    else:
        raise ValueError('Dimension should be 3 or 4')

    mean = np.mean(x, axis=axis, keepdims=True)
    std = np.std(x, axis=axis, keepdims=True)
    std_adj = np.maximum(std, 1.0/np.sqrt(size))
    y = (x - mean) / std_adj
    return y




def isotropically_resize_image(img, size, resample=cv2.INTER_AREA):
    h, w = img.shape[:2]
    if w > h:
        h = h * size // w
        w = size
    else:
        w = w * size // h
        h = size

    resized = cv2.resize(img, (w, h), interpolation=resample)
    return resized


def make_square_image(img):
    h, w = img.shape[:2]
    size = max(h, w)
    t = 0
    b = size - h
    l = 0
    r = size - w
    return cv2.copyMakeBorder(img, t, b, l, r, cv2.BORDER_CONSTANT, value=0)



def random_hflip(img, p=0.5):
    """Random horizontal flip."""
    if random.random() < p:
        return cv2.flip(img, 1)
    else:
        return img