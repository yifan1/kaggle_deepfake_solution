import os, sys, random
import numpy as np
import pandas as pd
import cv2

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.transforms import Normalize
from torch.utils.data import Dataset

from torch.utils.data import DataLoader
from tqdm.notebook import tqdm
from torchvision import models
from project_tools import project_config, project_utils
import shutil
import math
import pkbar
from efficientnet_pytorch import EfficientNet
from efficientnet_pytorch import model as enet
has_apex=True
try:
    from apex import amp
except:
    has_apex = False
    pass


class Unnormalize:
    """Converts an image tensor that was previously Normalize'd
    back to an image with pixels in the range [0, 1]."""
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, tensor):
        mean = torch.as_tensor(self.mean, dtype=tensor.dtype, device=tensor.device).view(3, 1, 1)
        std = torch.as_tensor(self.std, dtype=tensor.dtype, device=tensor.device).view(3, 1, 1)
        return torch.clamp(tensor*std + mean, 0., 1.)



mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]
normalize_transform = Normalize(mean, std)
unnormalize_transform = Unnormalize(mean, std)



def load_image_and_label(file_abs_path, cls, image_size, arg_func=None, convert_color=False):
    """Loads an image into a tensor. Also returns its label."""
    img = cv2.imread(os.path.join(file_abs_path))
    if convert_color:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    if arg_func is not None:
        img = arg_func([img])[0]

    img = cv2.resize(img, (image_size, image_size))

    img = torch.tensor(img).permute((2, 0, 1)).float().div(255)
    img = normalize_transform(img)

    target = cls
    # target = 1 if cls == "FAKE" else 0
    return img, target



class VideoDataset(Dataset):
    """Face crops dataset.

    Arguments:
        crops_dir: base folder for face crops
        df: Pandas DataFrame with metadata
        split: if "train", applies data augmentation
        image_size: resizes the image to a square of this size
        sample_size: evenly samples this many videos from the REAL
            and FAKE subfolders (None = use all videos)
         seed: optional random seed for sampling
    """

    def __init__(self, df, split, image_size, sample_size=None, seed=1234, arg_func=None):
        self.split = split
        self.image_size = image_size
        self.arg_func = arg_func

        if sample_size is not None:
            real_df = df[df["label"] == 1]
            fake_df = df[df["label"] == 0]
            num_real_faces = len(real_df)
            num_fake_faces = len(fake_df)

            # print(sample_size, len(real_df), len(fake_df))
            sample_size = np.min(np.array([sample_size, len(real_df), len(fake_df)]))
            real_df = real_df.sample(sample_size, random_state=seed)
            fake_df = fake_df.sample(sample_size, random_state=seed)
            num_original_videos = real_df['original'].nunique()
            num_fake_videos = fake_df['video'].nunique()

            print("%s: sampling %d from %d real faces from %d original videos" % (split, sample_size, num_real_faces, num_original_videos))
            print("%s: sampling %d from %d fake faces from %d fake videos" % (split, sample_size, num_fake_faces, num_fake_videos))


            self.df = pd.concat([real_df, fake_df])
        else:
            self.df = df

    def __getitem__(self, index):
        row = self.df.iloc[index]
        filename = row['abs_file']
        cls = row["label"]
        return load_image_and_label(filename, cls, self.image_size, arg_func=self.arg_func)

    def __len__(self):
        return len(self.df)




def create_data_loaders(df, split, image_size, batch_size, num_workers, sample_size=None, shuffle=True, seed=None, arg_func=None):
    dataset = VideoDataset(df, split, image_size, sample_size=sample_size, seed=seed, arg_func=arg_func)
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=shuffle,
                              num_workers=num_workers, pin_memory=True)
    return data_loader







class PytorchModel(nn.Module):
    def __init__(self, base, top=None, name='model', model_dir = '../models/pytorch/', networktype=None):
        super(PytorchModel, self).__init__()
        self.history = { "train_loss": [], "val_loss": [] }
        self.epochs_done = 0
        self.base = base
        self.top = top
        self.timestr = project_utils.get_time_string()
        self.loss = 1000.0
        self.model_dir = model_dir
        self.model_name = name
        self.timestamp = project_utils.get_time_string()
        model_file_base = model_dir + name + '_' + self.timestamp
        if not os.path.isfile(model_file_base + '.pth'):
            model_file_base = model_dir + name + '_' + self.timestamp
        else:
            model_file_base = model_dir + name + '_alt_' + self.timestamp
        self.model_file =  model_file_base + '.pth'
        self.network_type = networktype

    def forward(self, x):
        if (self.network_type=='efn') & (self.top is not None):
            out = self.base.extract_features(x)
            out = nn.AdaptiveAvgPool2d(1)(out)
        else:
            out = self.base(x)

        out = out.view(out.size(0), -1)
        if self.top is not None:
            out = self.top(out)
        return out

    def freeze_until(self, param_name):
        found_name = False
        for name, params in self.named_parameters():
            if name == param_name:
                found_name = True
            params.requires_grad = found_name

    def unfreeze_all(self):
        for name, params in self.named_parameters():
            params.requires_grad = True



def step_decay(epoch, initial_lr, drop=0.5, lr_drop_steps=2):
    epochs_drop = lr_drop_steps
    res_lr_rate = initial_lr * math.pow(drop, math.floor((1+epoch)/epochs_drop))
    return res_lr_rate



def fit(model, train_loader, val_loader, optimizer, device, epochs=5, lr_decay=True, drop=0.5, lr_drop_steps=3, patience=3, use_apex=False):
    print('start training')
    inital_lr = optimizer.param_groups[0]['lr']
    early_stopping_count = 0
    # with project_utils.mtqdm(total=len(train_loader), leave=False) as pbar:
    # with project_utils.mtqdm(total=len(train_loader)) as pbar:
    for epoch in range(epochs):
            # pbar.reset()
            # pbar.set_description("Epoch %d" % (model.epochs_done + 1))

        print('Epoch: %d/%d' % (epoch + 1, epochs))
        kbar = pkbar.Kbar(target=len(train_loader), width = 20)
        bce_loss = 0
        total_examples = 0

        if lr_decay:
            lr = step_decay(epoch, inital_lr, drop, lr_drop_steps)
            optimizer.param_groups[0]['lr'] = lr
            print('epoch %d, learning rate is %0.5f' % (epoch, optimizer.param_groups[0]['lr']))

        model.train(True)
        # for ii, data in project_utils.mtqdm(enumerate(train_loader), total=len(train_loader)):

        # internal steps within each epoch
        for ii, data in enumerate(train_loader):
            batch_size = data[0].shape[0]
            x = data[0].to(device)
            y_true = data[1].to(device).float()
            optimizer.zero_grad()
            y_pred = model(x)
            y_pred = y_pred.squeeze()
            loss = F.binary_cross_entropy_with_logits(y_pred, y_true)
            if use_apex & has_apex:
                with amp.scale_loss(loss, optimizer) as scaled_loss:
                    scaled_loss.backward()
            else:
                #                 Calculating gradients
                loss.backward()
            # loss.backward()
            optimizer.step()
            batch_bce = loss.item()
            bce_loss += batch_bce * batch_size
            model.history["train_loss"].append(batch_bce)
            kbar.update(ii, values=[("train_loss", batch_bce)])
            total_examples += batch_size
                # pbar.update()

        bce_loss /= total_examples
        model.epochs_done += 1

        # print("Epoch: %3d, train BCE: %.5f, " % (model.epochs_done, bce_loss), end=' ')
        val_bce_loss = evaluate(model, val_loader, device)[0]
        model.history["val_loss"].append(val_bce_loss)
        kbar.add(1, values=[("train_loss", bce_loss),
                            ("val_loss", val_bce_loss)
                            ])

        # print("val BCE: %.6f," % (val_bce_loss), end=' ')
        if val_bce_loss < model.loss:
            print('improve from %0.6f to %0.5f' %(model.loss, val_bce_loss))
            model.loss = val_bce_loss
            torch.save(model.state_dict(), model.model_file)
            early_stopping_count = 0
        else:
            early_stopping_count += 1
            print('validation loss has not improved, early stopping count is %d' % early_stopping_count)
            if early_stopping_count>=patience:
                print('early stopping')
                break

    model.load_state_dict(torch.load(model.model_file))
    final_model_file = model.model_dir + model.model_name + '_' + str(round(model.loss,6))+ '_' + model.timestamp + '.pth'
    try:
        shutil.move(model.model_file, final_model_file)
    except Exception as e:
        print(e)
    model.model_file = final_model_file
    # TODO: can do LR annealing here
    return None



def evaluate(model, val_loader, device, verbose=False):
    model.train(False)
    bce_loss = 0
    total_examples = 0
    y_preds = []
    for batch_idx, data in enumerate(val_loader):
        if verbose:
            kbar = pkbar.Kbar(target=len(val_loader), width = 20)
        with torch.no_grad():
            batch_size = data[0].shape[0]
            x = data[0].to(device)
            y_true = data[1].to(device).float()
            y_pred = model(x)
            y_pred = y_pred.squeeze()
            bce_loss += F.binary_cross_entropy_with_logits(y_pred, y_true).item() * batch_size
            y_preds += y_pred.tolist()
            total_examples += batch_size
        if verbose:
            kbar.update(batch_idx)

    if verbose:
        print(end='   ')
    bce_loss /= total_examples
    y_preds = torch.sigmoid(torch.Tensor(y_preds)).cpu().numpy()
    return bce_loss, y_preds








def torch_model1(checkpoint, device): #for st52, st54, st58
    networktype = 'efn'
    checkpoint = torch.load(checkpoint)
    base = EfficientNet.from_pretrained('efficientnet-b1', num_classes=1)
    model = PytorchModel(base=base, top=None, name='efnb1', networktype=None)
    model.load_state_dict(checkpoint)
    model.to(device)
    _ = model.eval()
    return model


def torch_model2(checkpoint, device, name='efnb1'):  # for st56, st57, st58, st59, st60, st61, st62, st63, st64, st65
    networktype = 'efn'
    checkpoint = torch.load(checkpoint)
    if name == 'efnb1':
        base_network = 'efficientnet-b1'
        base_out = 1280
    elif name == 'efnb3':
        base_network = 'efficientnet-b3'
        base_out = 1536
    elif name == 'efnb4':
        base_network = 'efficientnet-b4'
        base_out = 1792
    elif name == 'efnb5':
        base_network = 'efficientnet-b5'
        base_out = 2048

    base = EfficientNet.from_pretrained(base_network, num_classes=1)
    top = nn.Sequential(
        nn.Linear(base_out, 256),
        nn.ReLU(),
        nn.BatchNorm1d(256),
        nn.Dropout(0.3),
        nn.Linear(256, 32),
        nn.ReLU(),
        nn.BatchNorm1d(32),
        nn.Dropout(0.3),
        nn.Linear(32, 1)
    )
    model = PytorchModel(base=base, top=top, name=name, networktype=networktype)
    model.load_state_dict(checkpoint)
    model.to(device)
    _ = model.eval()
    return model




##### Qishen's classes/functions adaptation

class DeepFakeDataset(Dataset):
    def __init__(self, df, split, mode, image_size, mean_sub=0, transform=None, normalise=True, convert_color=False):

        self.df = df.reset_index(drop=True)
        self.split = split
        self.mode = mode
        self.image_size = image_size
        self.transform = transform
        self.normalise = normalise
        self.convert_color = convert_color

    def __len__(self):
        return self.df.shape[0]

    def __getitem__(self, index):

        row = self.df.iloc[index]
        file_path = row['abs_file']
        image = cv2.imread(file_path)
        if self.convert_color:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        image = cv2.resize(image, (self.image_size, self.image_size))
        if self.transform is not None:
            res = self.transform(image=image)
            image = res['image'].astype(np.float32)
        else:
            image = image.astype(np.float32)
        image /= 255
        image = image.transpose(2, 0, 1)
        image = torch.tensor(image)
        if self.normalise:
            image = normalize_transform(image)

        if self.mode == 'test':
            return image
        else:
            label = row["label"]
            return image, torch.tensor(label).float()




class enetv2(nn.Module):
    def __init__(self, backbone, out_dim=1):
        super(enetv2, self).__init__()
        self.enet = enet.EfficientNet.from_pretrained(backbone)
        self.dropouts = nn.ModuleList([
            nn.Dropout(0.5) for _ in range(5)
        ])
        self.myfc = nn.Linear(self.enet._fc.in_features, out_dim)
        self.enet._fc = nn.Identity()

    def forward(self, x):
        x = self.enet(x)
        for i, dropout in enumerate(self.dropouts):
            if i == 0:
                h = self.myfc(dropout(x))
            else:
                h += self.myfc(dropout(x))
        h /= len(self.dropouts)
        return h



def criterion(logits, targets):
    loss = nn.BCEWithLogitsLoss()(logits.view(-1), targets.view(-1))
    return loss



def train_epoch(model, loader, optimizer, device, use_amp=True):
    model.train()
    train_loss = []
    kbar = pkbar.Kbar(target=len(loader), width = 20)
    for ii, load_data in enumerate(loader):
        data, target = load_data[0].to(device), load_data[1].to(device)
        optimizer.zero_grad()
        logits = model(data)
        loss = criterion(logits, target)

        if not use_amp:
            loss.backward()
        else:
            with amp.scale_loss(loss, optimizer) as scaled_loss:
                scaled_loss.backward()
#         torch.nn.utils.clip_grad_norm_(model.parameters(), clip_grad)
        optimizer.step()
        loss_np = loss.detach().cpu().numpy()
        train_loss.append(loss_np)
        smooth_loss = sum(train_loss[-20:]) / min(len(train_loss), 20)
        kbar.update(ii, values=[("train_loss", loss_np), ("smooth_loss", smooth_loss)])
    return train_loss



def evaluate2(model, loader, device, get_output=False):
    model.eval()
    val_loss = []
    LOGITS = []
    acc = 0.0
    kbar = pkbar.Kbar(target=len(loader), width=20)
    with torch.no_grad():
        for ii, load_data in enumerate(loader):
            data, target = load_data[0].to(device), load_data[1].unsqueeze(-1).to(device)
            logits = model(data)
            LOGITS.append(logits)
            loss = criterion(logits, target)
            val_loss.append(loss.detach().cpu().numpy())
            pred = (logits > 0.5).float()
            acc += (target == pred).sum().cpu().numpy()
            kbar.update(ii, values=[("batch_val_loss", loss.detach().cpu().numpy())])
        val_loss = np.mean(val_loss)
        acc = acc / len(loader.dataset) * 100
    if get_output:
        # LOGITS = torch.cat(LOGITS).cpu().numpy()
        LOGITS = torch.sigmoid(torch.cat(LOGITS)).cpu().numpy()
        return LOGITS
    else:
        return val_loss, acc
