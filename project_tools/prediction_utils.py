import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import os
from os import listdir
from os.path import isfile, join, isdir

# import tensorflow as tf

try:
    import tensorflow.compat.v1 as tf
except:
    import tensorflow as tf

from tensorflow import keras as keras
import efficientnet
from efficientnet.tfkeras import init_tfkeras_custom_objects, preprocess_input
import cv2
import time
from contextlib import contextmanager
import matplotlib.pyplot as plt

import torch
import torch.nn as nn

from torchvision.transforms import Normalize
from scipy.stats.mstats import gmean

TF_VERSION = int(tf.__version__[0])

# utility function
@contextmanager
def timer(name):
    """
    utility timer function to check how long a piece of code might take to run.
    :param name: name of the code fragment to be timed
    :yield: time taken for the code to run
    """
    t0 = time.time()
    print('[%s] in progress' % name)
    yield
    print('[%s] done in %.0f s' %(name, time.time() - t0))


def get_folder_filelist(path, file_type=None):
    """
    take the path of a folder as input, output a list of files contained in the input folder
    :param path: file path of the input folder
    :param file_type: file type selector, allow to only output only files with certain extension, default value is 'html'
    :return: a list of file contained in the input folder
    """
    abs_files = []
    base_files = []
    if file_type is not None:
        types = file_type.split('|')
    for f in listdir(path):
        # print(f)
        if isfile(join(path,f)):
            if file_type is None:
                abs_files.append(join(path,f))
            else:
                ext = os.path.splitext(os.path.basename(f))[1][1:]
                if ext in types:
                    abs_files.append(join(path, f))
                    base_files.append(f)

        if isdir(join(path, f)):
            folder_abs_files, folder_base_files = get_folder_filelist(join(path,f), file_type=file_type)
            abs_files += folder_abs_files
            base_files += folder_base_files
    # print(len(abs_files))
    return abs_files, base_files




def get_single_image_array(file, grayscale=False, figsize=(3, 3), display=True, cvtcolor=False):
    """
    get the data array for a image by providing the full path of the image, and return the image array
    :param file: full path of the image
    :param grayscale: if True, display grayscale image; if False, display color image
    :param figsize: tuple that indicates the size of the figure area
    :param display: if True, display the image; if False, no display is triggered
    :return: cimg: the array representing the image
    """
    cmap_value = None
    img = cv2.imread(file)
    if grayscale:
        cimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    elif cvtcolor:
        cimg = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    else:
        cimg = img
    if grayscale: cmap_value = 'gray'
    if display:
        plt.figure(figsize=figsize)
        plt.axis("on")
        plt.imshow(cimg, cmap=cmap_value)
    return cimg


# Torch related function
class Unnormalize:
    """Converts an image tensor that was previously Normalize'd
    back to an image with pixels in the range [0, 1]."""
    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, tensor):
        mean = torch.as_tensor(self.mean, dtype=tensor.dtype, device=tensor.device).view(3, 1, 1)
        std = torch.as_tensor(self.std, dtype=tensor.dtype, device=tensor.device).view(3, 1, 1)
        return torch.clamp(tensor*std + mean, 0., 1.)



mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]
normalize_transform = Normalize(mean, std)
unnormalize_transform = Unnormalize(mean, std)



# def torch_process_image(img, device):
#     """Loads an image into a tensor. Also returns its label."""
#     img = torch.tensor(img).permute(().float().div(255)
#     img = normalize_transform(img).to(device)
#     return img


class FocalLoss(nn.Module):

    def __init__(self, gamma=0, eps=1e-7):
        super(FocalLoss, self).__init__()
        self.gamma = gamma
        self.eps = eps
        self.ce = torch.nn.CrossEntropyLoss()

    def forward(self, input, target):
        logp = self.ce(input, target)
        p = torch.exp(-logp)
        loss = (1 - p) ** self.gamma * logp
        return loss.mean()



# Pytorch model constructor
class PytorchModel(nn.Module):
    def __init__(self, base, top=None, name='model', networktype=None):
        super(PytorchModel, self).__init__()
        self.history = { "train_loss": [], "val_loss": [] }
        self.epochs_done = 0
        self.base = base
        self.top = top
        self.loss = 1000.0
        self.model_name = name

        self.network_type = networktype

    def forward(self, x):
        if (self.network_type=='efn') & (self.top is not None):
            out = self.base.extract_features(x)
            out = nn.AdaptiveAvgPool2d(1)(out)
        else:
            out = self.base(x)

        out = out.view(out.size(0), -1)
        if self.top is not None:
            out = self.top(out)
        return out



# SingleShot face detector
class TensoflowMobilNetSSDFaceDetector():
    def __init__(self,
                 det_threshold=0.3,
                 model_path='../models/mobilenet_ssd/frozen_inference_graph_face.pb'):

        self.det_threshold = det_threshold
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(model_path, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        with self.detection_graph.as_default():
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True
            self.sess = tf.Session(graph=self.detection_graph, config=config)

    def detect_face(self, image_np=None, image_file='', from_file=False):
        if from_file:
            print(image_file)
            image_np = get_single_image_array(image_file, display=False)
            print(image_np.shape)
            # image_np = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        h, w, c = image_np.shape

        image_np_expanded = np.expand_dims(image_np, axis=0)
        image_tensor = self.detection_graph.get_tensor_by_name(
            'image_tensor:0')

        boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')

        scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        classes = self.detection_graph.get_tensor_by_name(
            'detection_classes:0')
        num_detections = self.detection_graph.get_tensor_by_name(
            'num_detections:0')

        (boxes, scores, classes, num_detections) = self.sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})

        boxes = np.squeeze(boxes)
        scores = np.squeeze(scores)

        filtered_score_index = np.argwhere(
            scores >= self.det_threshold).flatten()
        selected_boxes = boxes[filtered_score_index]

        # faces = np.array([[
        #     int(x1 * w), # y1
        #     int(y1 * h), # y2
        #     int(x2 * w), # x1
        #     int(y2 * h), # x2
        # ] for x1, x2, y1, y2 in selected_boxes]) #y1, #y2, #x1, #x2

        faces = np.array([[
            int(y1 * w), # y1
            int(x1 * h), # y2
            int(y2 * w), # x1
            int(x2 * h), # x2
        ] for x1, y1, x2, y2,  in selected_boxes])


        return faces




# video prediction class
class Video_Predictor(object):
    def __init__(self, fd_model_dict, device):
        try:
            keras.backend.clear_session()
        except:
            pass
        self.cols = ['time', 'prediction']
        self.fd_model_dict = {}
        self.dims = set()
        self.device = device

        for key, value in fd_model_dict.items():
            self.fd_model_dict[key] = {}
            if fd_model_dict[key]['framework'] == 'torch':
                self.fd_model_dict[key]['framework'] = 'torch'
                self.fd_model_dict[key]['model'] = value['model']
                self.fd_model_dict[key]['dim'] = value['dim']


            else:
                print('loading model from file %s' % value['model_file'])
                model = keras.models.load_model(value['model_file'])
                self.fd_model_dict[key]['model'] = model
                self.fd_model_dict[key]['framework'] = 'keras'

                try:
                    dim = model.layers[0].input_shape[1]
                except:
                    dim = model.layers[0].input_shape[0][1]
                self.fd_model_dict[key]['dim'] = dim

            try:
                self.fd_model_dict[key]['model_type'] = value['model_type']
            except:
                self.fd_model_dict[key]['model_type'] = 'other'

            try:
                self.fd_model_dict[key]['weight'] = value['weight']
            except:
                pass

            try:
                self.fd_model_dict[key]['resize_opt'] = value['resize_opt']
            except:
                self.fd_model_dict[key]['resize_opt'] = 'cv2'

            try:
                self.fd_model_dict[key]['feature'] = value['feature']
            except:
                pass

    def video2faces_ssd(self, video_file, face_detector, n_parts=5, color_convert=False,
                        enlarge_ratio=1, size_limit = 5000):
        faces_in_video = []
        cap = cv2.VideoCapture(video_file)
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        if n_parts != 1:
            frame_ids = np.uint16(np.linspace(0, frame_count - 1, num=n_parts))
        else:
            frame_ids = [int(frame_count / 2)]
        for frame_index in range(int(frame_count)):
            ret = cap.grab()
            if frame_index in frame_ids:
                ret, video_frame = cap.retrieve()
                if color_convert:
                    video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB)

                faces = face_detector.detect_face(video_frame)
                if len(faces) > 0:
                    frame_faces = []
                    for face in faces:
                        x1 = face[0]
                        y1 = face[1]
                        x2 = face[2]
                        y2 = face[3]

                        if enlarge_ratio != 1:
                            org_x1 = x1
                            org_y1 = y1
                            org_w = x2 - x1
                            org_h = y2 - y1
                            cx = org_x1 + int(org_w / 2)
                            cy = org_y1 + int(org_h / 2)
                            max_x = video_frame.shape[1] - 1
                            max_y = video_frame.shape[0] - 1
                            nw = int(org_w * enlarge_ratio)
                            nh = int(org_h * enlarge_ratio)
                            x1 = cx - int(nw / 2)
                            y1 = cy - int(nh / 2)
                            x2 = x1 + nw
                            y2 = y1 + nh
                            x1 = x1 if x1 >= 0 else 0
                            x2 = x2 if x1 <= max_x else max_x
                            y1 = y1 if y1 >= 0 else 0
                            y2 = y2 if y2 <= max_y else max_y
                        crop_img = video_frame[y1:y2, x1:x2, :]
                        h = y2-y1
                        w = x2-x1
                        if h*w > size_limit:
                            frame_faces.append(crop_img)
                    faces_in_video += frame_faces
        cap.release()
        return faces_in_video



    def predict_video_byface(self, video_file, face_detector, n_parts=5, enlarge_ratio=1.3, ensemble_option='gmean'):
        faces_in_video =self.video2faces_ssd(video_file, face_detector=face_detector,
                                             n_parts=n_parts, enlarge_ratio=enlarge_ratio)
        result_df = pd.DataFrame(columns=self.cols)
        # print(len(faces_in_video))
        if len(faces_in_video) > 0:
            if ensemble_option!='wavg': #weighted average
                predictions = np.zeros((len(faces_in_video), len(self.fd_model_dict)))
                model_count=0
            else:
                predictions = np.zeros(len(faces_in_video))
            for key, value in self.fd_model_dict.items():
                imgs = []
                model = value['model']
                dim = value['dim']
                if value['framework'] != 'torch':
                    if value['model_type'] == 'efn':
                        preprocess_func = efficientnet.tfkeras.preprocess_input
                    elif value['model_type'] == 'resnet':
                        preprocess_func = tf.keras.applications.resnet.preprocess_input
                    elif value['model_type'] == 'xception':
                        preprocess_func = tf.keras.applications.xception.preprocess_input
                    elif value['model_type'] == 'densenet':
                        preprocess_func = tf.keras.applications.densenet.preprocess_input
                    else:
                        preprocess_func = lambda x: x/255.0
                    for face in faces_in_video:
                        img = cv2.resize(face, dsize=(dim, dim))
                        imgs.append(img)
                    imgs=np.array(imgs)
                    imgs = preprocess_func(imgs)
                    model_pred = model.predict(imgs).flatten()

                else: #value['framework'] == 'pytorch':
                    for face in faces_in_video:
                        img = cv2.resize(face, dsize=(dim, dim))
                        imgs.append(img)
                    imgs=np.array(imgs)
                    imgs = torch.tensor(imgs, device=self.device).float()
                    imgs = imgs.permute((0, 3, 1, 2))
                    for i in range(len(imgs)):
                            imgs[i] = normalize_transform(imgs[i] / 255.)
                    with torch.no_grad():
                        model_pred = torch.sigmoid(model(imgs).squeeze())
                    model_pred = model_pred.cpu().numpy()
                if ensemble_option!='wavg':
                    predictions[:, model_count] = model_pred
                    model_count += 1
                else:
                    predictions += model_pred * value['weight']

            if ensemble_option=='gmean':
                result_df['prediction'] = gmean(predictions, axis=1)
            elif ensemble_option=='amean': #arithmetic mean
                result_df['prediction'] = np.mean(predictions, axis=1)
            else:
                result_df['prediction'] = predictions
            result_df['time'] = result_df.index.values
        else:
            result_df['prediction'] = [0.5]
            result_df['time'] = [0]
        return result_df






