import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from os import listdir
from project_tools import project_config, project_utils, project_class, prediction_utils
import re
import cv2
import os

def keras_model_evaluation(val_df, model_file, thresholds=[]):
    print('loading model %s' % model_file)
    model1 = keras.models.load_model(model_file)
    dim = model1.layers[0].input_shape[1]
    print('input shape is %d' % dim)

    df_gen = project_class.DataFrame_Generator(val_df, dim=dim, aug_func=None, shuffle=False)

    batch_size = 256
    steps = (len(val_df)//batch_size)+1
    # print('step is %d' % steps)
    y_pred = model1.predict_generator(df_gen.flow(batch_size=batch_size), steps=steps, verbose=1)[0:len(val_df)]

    # val_img_list = val_df['abs_file'].values
    # x_valid = (project_utils.generate_image_array(val_img_list, size=(INPUT_DIM, INPUT_DIM)) / 255.0)
    # y_pred = model1.predict(x_valid, verbose=1)

    outputs = []
    if len(thresholds)>0:
        for tv in thresholds:
            print(tv)
            outputs.append(project_utils.prediction_evaluation(val_df, y_pred, tv))

    return outputs, y_pred, model1



def generate_baseline_model_benchmark_df(val_df, files, data_version, thresholds=[]):
    patrn = r'st[0-9]+'
    df_dict = {}
    for i in range(len(files)):
        file = files[i]
        output_items = keras_model_evaluation(val_df, file, thresholds=thresholds)[0]
        try:
            st = re.findall(patrn, file)[0]
            for output in output_items:
                if len(df_dict) == 0:
                    df_dict['st'] = []
                    df_dict['val_data_ver'] = []
                    for key in output.keys():
                        df_dict[key] = []
                df_dict['st'].append(st)
                df_dict['val_data_ver'].append(data_version)
                for key in output.keys():
                    df_dict[key].append(output[key])
        except:
            pass
        keras.backend.clear_session()
    df = pd.DataFrame.from_dict(df_dict)
    return df



class DataFrame_Sequence(keras.utils.Sequence):
    def __init__(self, df, dim, batch_size, shuffle=True, aug_func = None, preprocess_func=lambda x: x/255.0, file_field='abs_file'):
        self.df = df
        self.img_list = df[file_field]
        self.label = df['label'].values
        self.size = (dim, dim)
        self.iter_ndx = np.arange(len(self.df))
        self.aug_func = aug_func
        self.batch_size = batch_size
        self.preprocess_func = preprocess_func
        self.read_count = 0

        if shuffle:
            np.random.shuffle(self.iter_ndx)

        # self.iter_ndx = cycle(self.iter_ndx)
        # self.current_idx = -1

    def __len__(self):
        return int(np.ceil(len(self.df) / self.batch_size))

    def __getitem__(self, idx):
        idxs = self.iter_ndx[idx*self.batch_size:(idx+1)*self.batch_size]

        img_batch = []
        y_batch = []
        # id_batch = []
        for idx in idxs:
                img_item = self.img_list[idx]
                y_item= self.label[idx]
                img_batch.append(img_item)
                y_batch.append(y_item)
                # id_batch.append(idx)

        y_batch = np.stack(y_batch, axis=0)
        x_batch = project_utils.generate_image_array(img_batch, size=self.size)
        if self.aug_func is not None:
            x_batch = self.aug_func(x_batch)
        x_batch = self.preprocess_func(x_batch)
        return (x_batch, y_batch)




class VideoFaceExtractor(object):
    def __init__(self):
        try:
            keras.backend.clear_session()
        except:
            pass

    def video2faces_ssd(self, video_file, face_detector, face_img_dir, n_parts=2, color_convert=False,
                        enlarge_ratio=1):

        if not (os.path.isdir(face_img_dir)):
            os.mkdir(face_img_dir)

        basename = os.path.basename(video_file)[:-4]
        faces_in_video = []
        cap = cv2.VideoCapture(video_file)
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        res_files = []

        if n_parts != 1:
            frame_ids = np.uint16(np.linspace(0, frame_count - 1, num=n_parts))
        else:
            frame_ids = [int(frame_count / 2)]
        for frame_index in range(int(frame_count)):
            ret = cap.grab()
            if frame_index in frame_ids:
                ret, video_frame = cap.retrieve()
                if color_convert:
                    video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2RGB)

                faces = face_detector.detect_face(video_frame)
                if len(faces) > 0:
                    frame_faces = []
                    face_id = 1
                    for face in faces:
                        x1 = face[0]
                        y1 = face[1]
                        x2 = face[2]
                        y2 = face[3]

                        if enlarge_ratio != 1:
                            org_x1 = x1
                            org_y1 = y1
                            org_w = x2 - x1
                            org_h = y2 - y1
                            cx = org_x1 + int(org_w / 2)
                            cy = org_y1 + int(org_h / 2)
                            max_x = video_frame.shape[1] - 1
                            max_y = video_frame.shape[0] - 1
                            nw = int(org_w * enlarge_ratio)
                            nh = int(org_h * enlarge_ratio)
                            x1 = cx - int(nw / 2)
                            y1 = cy - int(nh / 2)
                            x2 = x1 + nw
                            y2 = y1 + nh
                            x1 = x1 if x1 >= 0 else 0
                            x2 = x2 if x1 <= max_x else max_x
                            y1 = y1 if y1 >= 0 else 0
                            y2 = y2 if y2 <= max_y else max_y

                        crop_img = video_frame[y1:y2, x1:x2, :]
                        face_str = 'f' + str(len(faces)) + 'id' + str(face_id)
                        dst_file = face_img_dir + basename + '_' + face_str + '.jpg'
                        cv2.imwrite(dst_file, crop_img)
                        res_files.append(dst_file)
                        face_id += 1
        cap.release()
        return res_files






