import numpy as np
from project_tools import project_config, project_utils, project_class
from itertools import cycle


try:
    import tensorflow.compat.v1 as tf
except:
    import tensorflow as tf

class DataFrame_Generator(object):
    def __init__(self, df, dim, shuffle=True, aug_func = None, file_field='abs_file'):
        self.df = df
        self.img_list = df[file_field]
        self.label = df['label'].values
        self.size = (dim, dim)
        self.iter_ndx = np.arange(len(self.df))
        self.aug_func = aug_func
        self.read_count = 0

        if shuffle:
            np.random.shuffle(self.iter_ndx)

        self.iter_ndx = cycle(self.iter_ndx)
        self.current_idx = -1

    def flow(self, batch_size):
        while True:
            img_batch = []
            y_batch = []
            id_batch = []
            for i in range(batch_size):
                self.current_idx = next(self.iter_ndx)
                # print(self.current_idx)
                img_item = self.img_list[self.current_idx]
                y_item= self.label[self.current_idx]
                img_batch.append(img_item)
                y_batch.append(y_item)
                id_batch.append(self.current_idx)

            y_batch = np.stack(y_batch, axis=0)
            x_batch = project_utils.generate_image_array(img_batch, size=self.size)
            if self.aug_func is not None:
                x_batch = self.aug_func(x_batch)/255.0
            else:
                x_batch = x_batch/255.0
            yield (x_batch, y_batch)




