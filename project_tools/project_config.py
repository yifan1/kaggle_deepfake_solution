import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))


INPUT_DIM=299

IMG_DIR ='../input/train_images/'
# ORIGINAL_IMAGE_DIR = '../input/KnivesImagesDatabase/'

TOTAL_ORIGINAL = 19154

SCRIPT_BACKUP_PATH = '../backup/scripts_backup/'

TRAIN_GROUP_MAP = {
                   'train_sample_videos':'trnsamp', 'check_videos':'chkvid',
                   'dfdc_train_part_0':'trnp00', 'dfdc_train_part_1': 'trnp01',
                   'dfdc_train_part_2': 'trnp02', 'dfdc_train_part_3': 'trnp03',
                   'dfdc_train_part_4': 'trnp04', 'dfdc_train_part_5': 'trnp05',
                   'dfdc_train_part_6': 'trnp06', 'dfdc_train_part_7': 'trnp07',
                   'dfdc_train_part_8': 'trnp08', 'dfdc_train_part_9': 'trnp09',

                   'dfdc_train_part_10': 'trnp10', 'dfdc_train_part_11': 'trnp11',
                   'dfdc_train_part_12': 'trnp12', 'dfdc_train_part_13': 'trnp13',
                   'dfdc_train_part_14': 'trnp14', 'dfdc_train_part_15': 'trnp15',
                   'dfdc_train_part_16': 'trnp16', 'dfdc_train_part_17': 'trnp17',
                   'dfdc_train_part_18': 'trnp18', 'dfdc_train_part_19': 'trnp19',

                   'dfdc_train_part_20': 'trnp20', 'dfdc_train_part_21': 'trnp21',
                   'dfdc_train_part_22': 'trnp22', 'dfdc_train_part_23': 'trnp23',
                   'dfdc_train_part_24': 'trnp24', 'dfdc_train_part_25': 'trnp25',
                   'dfdc_train_part_26': 'trnp26', 'dfdc_train_part_27': 'trnp27',
                   'dfdc_train_part_28': 'trnp28', 'dfdc_train_part_29': 'trnp29',

                   'dfdc_train_part_30': 'trnp30', 'dfdc_train_part_31': 'trnp31',
                   'dfdc_train_part_32': 'trnp32', 'dfdc_train_part_33': 'trnp33',
                   'dfdc_train_part_34': 'trnp34', 'dfdc_train_part_35': 'trnp35',
                   'dfdc_train_part_36': 'trnp36', 'dfdc_train_part_37': 'trnp37',
                   'dfdc_train_part_38': 'trnp38', 'dfdc_train_part_39': 'trnp39',

                   'dfdc_train_part_40': 'trnp40', 'dfdc_train_part_41': 'trnp41',
                   'dfdc_train_part_42': 'trnp42', 'dfdc_train_part_43': 'trnp43',
                   'dfdc_train_part_44': 'trnp44', 'dfdc_train_part_45': 'trnp45',
                   'dfdc_train_part_46': 'trnp46', 'dfdc_train_part_47': 'trnp47',
                   'dfdc_train_part_48': 'trnp48', 'dfdc_train_part_49': 'trnp49',

                   }



VALIDATION_GROUP1 = ['trnp05', 'trnp10', 'trnp20', 'trnp22', 'trnp24', 'trnp26', 'trnp29', 'trnp32', 'trnp35', 'trnp40', 'trnp42', 'trnp48', 'trnp49']
VALIDATION_GROUP2 = ['trnp09', 'trnp33', 'trnp10']
VALIDATION_GROUP3 = ['trnp01', 'trnp02', 'trnp07', 'trnp15', 'trnp20', 'trnp21', 'trnp24', 'trnp29', 'trnp31', 'trnp35', 'trnp37', 'trnp47', 'trnp49']



# Model parameters for private LB 0.42x solution 
MODEL_PARAMS = {}

MODEL_PARAMS['st104model'] = {}
MODEL_PARAMS['st104model']['train_size'] = 300000
MODEL_PARAMS['st104model']['validation_size'] = 79360
MODEL_PARAMS['st104model']['validation_group'] = VALIDATION_GROUP2
MODEL_PARAMS['st104model']['additional_train_size'] = 0 
MODEL_PARAMS['st104model']['additional_validation_size'] = 0 
MODEL_PARAMS['st104model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st104model']['img_cond_value'] = 5000
MODEL_PARAMS['st104model']['architecture'] = 'efnb1'
MODEL_PARAMS['st104model']['batch_size'] = 64
MODEL_PARAMS['st104model']['remove_fake_audio'] = False



MODEL_PARAMS['st105model'] = {}
MODEL_PARAMS['st105model']['train_size'] = 150000
MODEL_PARAMS['st105model']['validation_size'] = 79360
MODEL_PARAMS['st105model']['validation_group'] = VALIDATION_GROUP2
MODEL_PARAMS['st105model']['additional_train_size'] = 0 
MODEL_PARAMS['st105model']['additional_validation_size'] = 0 
MODEL_PARAMS['st105model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st105model']['img_cond_value'] = 5000
MODEL_PARAMS['st105model']['architecture'] = 'efnb1'
MODEL_PARAMS['st105model']['batch_size'] = 64
MODEL_PARAMS['st105model']['remove_fake_audio'] = False


MODEL_PARAMS['st106model'] = {}
MODEL_PARAMS['st106model']['train_size'] = 200000
MODEL_PARAMS['st106model']['validation_size'] = 79360
MODEL_PARAMS['st106model']['validation_group'] = VALIDATION_GROUP2
MODEL_PARAMS['st106model']['additional_train_size'] = 70000
MODEL_PARAMS['st106model']['additional_validation_size'] = 0 
MODEL_PARAMS['st106model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st106model']['img_cond_value'] = 5000
MODEL_PARAMS['st106model']['architecture'] = 'efnb1'
MODEL_PARAMS['st106model']['batch_size'] = 64
MODEL_PARAMS['st106model']['remove_fake_audio'] = False


MODEL_PARAMS['st111model'] = {}
MODEL_PARAMS['st111model']['train_size'] = 150000
MODEL_PARAMS['st111model']['validation_size'] = 79360
MODEL_PARAMS['st111model']['validation_group'] = VALIDATION_GROUP2
MODEL_PARAMS['st111model']['additional_train_size'] = 0 
MODEL_PARAMS['st111model']['additional_validation_size'] = 0 
MODEL_PARAMS['st111model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st111model']['img_cond_value'] = 5000
MODEL_PARAMS['st111model']['architecture'] = 'efnb4'
MODEL_PARAMS['st111model']['batch_size'] = 32
MODEL_PARAMS['st111model']['remove_fake_audio'] = False


MODEL_PARAMS['st112model'] = {}
MODEL_PARAMS['st112model']['train_size'] = 150000
MODEL_PARAMS['st112model']['validation_size'] = 100000
MODEL_PARAMS['st112model']['validation_group'] = VALIDATION_GROUP1
MODEL_PARAMS['st112model']['additional_train_size'] = 0 
MODEL_PARAMS['st112model']['additional_validation_size'] = 0 
MODEL_PARAMS['st112model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st112model']['img_cond_value'] = 5000
MODEL_PARAMS['st112model']['architecture'] = 'efnb1'
MODEL_PARAMS['st112model']['batch_size'] = 64
MODEL_PARAMS['st112model']['remove_fake_audio'] = False


MODEL_PARAMS['st113model'] = {}
MODEL_PARAMS['st113model']['train_size'] = 150000
MODEL_PARAMS['st113model']['validation_size'] = 100000
MODEL_PARAMS['st113model']['validation_group'] = VALIDATION_GROUP3
MODEL_PARAMS['st113model']['additional_train_size'] = 0 
MODEL_PARAMS['st113model']['additional_validation_size'] = 0 
MODEL_PARAMS['st113model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st113model']['img_cond_value'] = 5000
MODEL_PARAMS['st113model']['architecture'] = 'efnb4'
MODEL_PARAMS['st113model']['batch_size'] = 32
MODEL_PARAMS['st113model']['remove_fake_audio'] = False


MODEL_PARAMS['st119model'] = {}
MODEL_PARAMS['st119model']['train_size'] = 150000
MODEL_PARAMS['st119model']['validation_size'] = 100000
MODEL_PARAMS['st119model']['validation_group'] = VALIDATION_GROUP1
MODEL_PARAMS['st119model']['additional_train_size'] = 200000
MODEL_PARAMS['st119model']['additional_validation_size'] = 0 
MODEL_PARAMS['st119model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st119model']['img_cond_value'] = 5000
MODEL_PARAMS['st119model']['architecture'] = 'efnb0'
MODEL_PARAMS['st119model']['batch_size'] = 128
MODEL_PARAMS['st119model']['remove_fake_audio'] = True


MODEL_PARAMS['st120model'] = {}
MODEL_PARAMS['st120model']['train_size'] = 150000
MODEL_PARAMS['st120model']['validation_size'] = 79360
MODEL_PARAMS['st120model']['validation_group'] = VALIDATION_GROUP2
MODEL_PARAMS['st120model']['additional_train_size'] = 200000 
MODEL_PARAMS['st120model']['additional_validation_size'] = 0 
MODEL_PARAMS['st120model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st120model']['img_cond_value'] = 5000
MODEL_PARAMS['st120model']['architecture'] = 'efnb4'
MODEL_PARAMS['st120model']['batch_size'] = 32
MODEL_PARAMS['st120model']['remove_fake_audio'] = False


MODEL_PARAMS['st121model'] = {}
MODEL_PARAMS['st121model']['train_size'] = 150000
MODEL_PARAMS['st121model']['validation_size'] = 100000
MODEL_PARAMS['st121model']['validation_group'] = VALIDATION_GROUP1
MODEL_PARAMS['st121model']['additional_train_size'] = 200000
MODEL_PARAMS['st121model']['additional_validation_size'] = 0 
MODEL_PARAMS['st121model']['img_cond_feature'] = 'size'
MODEL_PARAMS['st121model']['img_cond_value'] = 5000
MODEL_PARAMS['st121model']['architecture'] = 'efnb0'
MODEL_PARAMS['st121model']['batch_size'] = 128
MODEL_PARAMS['st121model']['remove_fake_audio'] = False


MODEL_PARAMS['st123model'] = {}
MODEL_PARAMS['st123model']['train_size'] = 150000
MODEL_PARAMS['st123model']['validation_size'] = 100000
MODEL_PARAMS['st123model']['validation_group'] = VALIDATION_GROUP1
MODEL_PARAMS['st123model']['additional_train_size'] = 200000
MODEL_PARAMS['st123model']['additional_validation_size'] = 0 
MODEL_PARAMS['st123model']['img_cond_feature'] = 'goodface_prob'
MODEL_PARAMS['st123model']['img_cond_value'] = 0.5
MODEL_PARAMS['st123model']['architecture'] = 'efnb0'
MODEL_PARAMS['st123model']['batch_size'] = 128
MODEL_PARAMS['st123model']['remove_fake_audio'] = False


MODEL_PARAMS['st124model'] = {}
MODEL_PARAMS['st124model']['train_size'] = 150000
MODEL_PARAMS['st124model']['validation_size'] = 79360
MODEL_PARAMS['st124model']['validation_group'] = VALIDATION_GROUP2
MODEL_PARAMS['st124model']['additional_train_size'] = 200000
MODEL_PARAMS['st124model']['additional_validation_size'] = 0 
MODEL_PARAMS['st124model']['img_cond_feature'] = 'goodface_prob'
MODEL_PARAMS['st124model']['img_cond_value'] = 0.5
MODEL_PARAMS['st124model']['architecture'] = 'efnb0'
MODEL_PARAMS['st124model']['batch_size'] = 128
MODEL_PARAMS['st124model']['remove_fake_audio'] = False


MODEL_PARAMS['st125model'] = {}
MODEL_PARAMS['st125model']['train_size'] = 150000
MODEL_PARAMS['st125model']['validation_size'] = 100000
MODEL_PARAMS['st125model']['validation_group'] = VALIDATION_GROUP1
MODEL_PARAMS['st125model']['additional_train_size'] = 200000 
MODEL_PARAMS['st125model']['additional_validation_size'] = 0 
MODEL_PARAMS['st125model']['img_cond_feature'] = 'goodface_prob'
MODEL_PARAMS['st125model']['img_cond_value'] = 0.5
MODEL_PARAMS['st125model']['architecture'] = 'efnb1'
MODEL_PARAMS['st125model']['batch_size'] = 64
MODEL_PARAMS['st125model']['remove_fake_audio'] = False


MODEL_PARAMS['st126model'] = {}
MODEL_PARAMS['st126model']['train_size'] = 150000
MODEL_PARAMS['st126model']['validation_size'] = 79360
MODEL_PARAMS['st126model']['validation_group'] = VALIDATION_GROUP2
MODEL_PARAMS['st126model']['additional_train_size'] = 200000
MODEL_PARAMS['st126model']['additional_validation_size'] = 0 
MODEL_PARAMS['st126model']['img_cond_feature'] = 'goodface_prob'
MODEL_PARAMS['st126model']['img_cond_value'] = 0.5
MODEL_PARAMS['st126model']['architecture'] = 'efnb1'
MODEL_PARAMS['st126model']['batch_size'] = 64
MODEL_PARAMS['st126model']['remove_fake_audio'] = False


MODEL_PARAMS['mh3model'] = {}
MODEL_PARAMS['mh3model']['train_size'] = 200000
MODEL_PARAMS['mh3model']['validation_size'] = 100000
MODEL_PARAMS['mh3model']['validation_group'] = VALIDATION_GROUP1
MODEL_PARAMS['mh3model']['additional_train_size'] = 100000
MODEL_PARAMS['mh3model']['additional_validation_size'] = 0 
MODEL_PARAMS['mh3model']['img_cond_feature'] = 'size'
MODEL_PARAMS['mh3model']['img_cond_value'] = 5000
MODEL_PARAMS['mh3model']['architecture'] = 'efnb3'
MODEL_PARAMS['mh3model']['batch_size'] = 32
MODEL_PARAMS['mh3model']['remove_fake_audio'] = False

