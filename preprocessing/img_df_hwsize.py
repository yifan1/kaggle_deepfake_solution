import numpy as np
import pandas as pd
import string
import re
import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
from project_tools import project_utils
# from multiprocessing import Pool
# from functools import partial
from concurrent.futures import ThreadPoolExecutor

img_df = project_utils.load_data('../input/imge_df_4538424row.pkl')

# sample = img_df.sample(100000).reset_index(drop=True)

def get_image_size_mp(files, num_workers):
    def get_image_size(i):
        file = files[i]
        img = project_utils.get_single_image_array(file, display=False)
        w = img.shape[0]
        h = img.shape[1]
        return w, h
    with ThreadPoolExecutor(max_workers=num_workers) as ex:
        size = ex.map(get_image_size, range(len(files)))
    return list(size)


# res = get_image_size(sample['abs_file'].values[0])
sizes = get_image_size_mp(img_df['abs_file'].tolist(), 24)


res_df = pd.DataFrame()
res_df['abs_file'] = img_df['abs_file']
# res_df['size'] = sizes
res_df['width'], res_df['height'] = map(list, zip(*sizes)) 
res_df['size'] = res_df['width'] * res_df['height']



# img_df['width'] = res_df['width']
# img_df['height'] = res_df['height']
# img_df['size'] = res_df['size']

# project_utils.pickle_data('../input/imge_df_4538424row_v2.pkl', img_df)


# cond = (res_df['size'] < 10000) #& (res_df['size'] > 2000)
# small_img_df = res_df[cond].reset_index(drop=True)
# print(small_img_df.shape)


# def random_display(df, n=10):
#     for i in df.sample(n).index:
#         project_utils.get_single_image_array(df['abs_file'].values[i], display=True)


# random_display(small_img_df, n=5)



# lcond = (res_df['size'] > 500000) #& (res_df['size'] > 2000)
# large_img_df = res_df[lcond].reset_index(drop=True)
# print(large_img_df.shape)
