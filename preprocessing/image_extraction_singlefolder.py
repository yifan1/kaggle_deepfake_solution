import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
from project_tools import project_utils, project_class, project_config

import warnings
warnings.filterwarnings("ignore")

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-s', '--src',help='source directory')
parser.add_argument('-d', '--dst',help='destination directory')
parser.add_argument('-n', '--nparts',help='number of parts')

args = parser.parse_args()
video_folder = args.src
image_folder = args.dst if (args.dst is not None) else '../input/train_images/'
n_parts = int(args.nparts) if (args.nparts is not None) else 40

# usage %run image_extraction_singlefolder.py -s '../hdd1_link/input/train_videos/dfdc_train_part_n/' -d '../hdd1_link/input/train_images_pn/'
res = project_utils.extract_images_from_videos(video_folder, image_folder, n_parts=n_parts)


