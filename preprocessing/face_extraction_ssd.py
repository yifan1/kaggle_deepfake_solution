import numpy as np
import pandas as pd
import string
import re
import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
from project_tools import project_utils, project_class, project_config, prediction_utils


src_folder = '../data/train_frames/'
dst_folder = '../data/train_faces/'


project_utils.reload_project()
crop_imgs = []
color_miss_list = []
enlarge_ratio = 1.3


sdd_model_path = '../models/mobilenet_ssd/frozen_inference_graph_face.pb'
image_list, basenames = project_utils.glob_folder_filelist(src_folder)



print('extracting face from %d files' % len(image_list))

project_utils.reload_project()
face_detector = prediction_utils.TensoflowMobilNetSSDFaceDetector(model_path=sdd_model_path)
with project_utils.timer('check crop time'):
    mtq = project_utils.mtqdm(image_list)
    for file in mtq:        
        crop_imgs = project_utils.ssd_facedetector(file, face_detector, dst_folder, enlarge_ratio=enlarge_ratio)
