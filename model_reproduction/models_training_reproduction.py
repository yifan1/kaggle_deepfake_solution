import numpy as np
import pandas as pd
import string
import re
import os
import sys
sys.path.append(os.path.dirname(os.getcwd()))
from project_tools import project_utils, project_class, project_config, torch_utils

import json
import glob
import time
import cv2
import PIL.Image
import matplotlib.pyplot as plt
import shutil

from tqdm import tqdm_notebook as tqdm
from sklearn.metrics import log_loss
from sklearn.model_selection import train_test_split, StratifiedKFold
import torch
from torch.utils.data import TensorDataset, DataLoader,Dataset
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data.sampler import SubsetRandomSampler, RandomSampler, SequentialSampler
from torch.optim.lr_scheduler import StepLR, ReduceLROnPlateau, CosineAnnealingLR
from warmup_scheduler import GradualWarmupScheduler  # https://github.com/ildoonet/pytorch-gradual-warmup-lr
import albumentations
import matplotlib.pyplot as plt
import seaborn as sns
import pkbar

try:
    from apex import amp
    use_apex = True
except:
    use_apex = False
    
from efficientnet_pytorch import model as enet

from project_tools import project_utils, project_class, project_config, torch_utils


import argparse


# running specific model example: models_training_reproduction.py -m st104model -g 0
# help: models_training_reproduction.py --help


parser = argparse.ArgumentParser(description='model training')
parser.add_argument('-m', '--model_id',help='model configuration')
parser.add_argument('-g', '--gpu_id', help='gpu selection')
args = parser.parse_args()


model_id = args.model_id
gpu_id = args.gpu_id


# retrieve model parameter and GPU identification, if model_id isn't correct, terminate the script
if model_id in project_config.MODEL_PARAMS.keys():
    print('retraining model %s with GPU %s' % (model_id, gpu_id))
    model_params = project_config.MODEL_PARAMS[model_id]
else:
    print('model_id %s not recognised, terminating...' % model_id)
    sys.exit()



# identify GPU
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = gpu_id

# identify type of efficientnet to run 
network_dict ={}
network_dict['efnb0'] = 'efficientnet-b0'
network_dict['efnb1'] = 'efficientnet-b1'
network_dict['efnb2'] = 'efficientnet-b2'
network_dict['efnb3'] = 'efficientnet-b3'
network_dict['efnb4'] = 'efficientnet-b4'
network_dict['efnb5'] = 'efficientnet-b5'
network_name = model_params['architecture']
enet_type = network_dict[network_name]




# set model ouput name, and other model parameters
time_str = project_utils.get_time_string()
kernel_type = '../models/pytorch/' + model_id + '_' + network_name + '_' + time_str
backup_script = False
opt_module = optim.AdamW
image_size = 256
cut_size = int(image_size * 0.80)
mean_sub = 0
batch_size = model_params['batch_size']
num_workers = 16

# set learning rate, and epochs number of different training stages
init_lr = 0.0001
freeze_epo = 0
warmup_epo = 1
cosine_epo = 30
n_epochs = freeze_epo + warmup_epo + cosine_epo


# setting parameters for train/valid seperation
holdout_groups = model_params['validation_group']
stratified_feature = 'group'
train_ratio = 0.75
use_baseline_valid = False
use_baseline_train = False

sample_valid = True
sample_valid_size = model_params['validation_size']

sample_train = True
sample_train_size = model_params['train_size']

sample_additional_train_size = model_params['additional_train_size']
sample_additional_valid_size = model_params['additional_validation_size']

img_cond_feature = model_params['img_cond_feature']
img_cond_value = model_params['img_cond_value']
remove_fake_audio = model_params['remove_fake_audio']

img_df_file = '../data/img_df.pkl'
additional_df_file = '../data/additional_df.pkl'
baseline = None  #'../models/pytorch/qishen_adaptation_efnb1_202003141812_val0.294983_meta.pkl'

av_labels_file = '../data/audio_video_labels.csv'
av_label = pd.read_csv(av_labels_file)
fake_audios = av_label[(av_label['audio_label']=='FAKE')]['filename'].tolist()



def df_balanced_sampling(df, sample_size, adf_delta=0):
    if sample_size > int(len(df)):
        if not oversample:
            true_idx = df[df['label']==1].index.tolist()  
            fake_idx = df[df['label']==0].index.tolist()
            if len(true_idx) < len(fake_idx):
                fake_idx = df[df['label']==0].sample(len(true_idx),replace=True).index.tolist()
            else:
                true_idx = df[df['label']==1].sample(len(fake_idx), replace=True).index.tolist()
        else:
            print('sample size > df size, perform oversampling')
            fake_idx = df[df['label']==0].sample(int(sample_size/2), replace=True).index.tolist()
            true_idx = df[df['label']==1].sample(int(sample_size/2), replace=True).index.tolist()            
    else:
        fake_idx = df[df['label']==0].sample(int(sample_size/2)+adf_delta, replace=True).index.tolist()
        true_idx = df[df['label']==1].sample(int(sample_size/2), replace=True).index.tolist()
        print('fake %d, true %d, adf_delta %d' % (len(fake_idx), len(true_idx), adf_delta))
    df = df.loc[(true_idx + fake_idx)].reset_index(drop=True)
    return df

def get_dataframes():
    global img_df_file, baseline, stratified_feature, additional_df_file, train_ratio
    global sample_valid, sample_valid_size, sample_train, sample_train_size, img_cond_feature, img_cond_value
    global use_baseline_train, use_baseline_valid, holdout_groups, remove_fake_audio 
    global sample_additional_train_size, sample_additional_valid_size
    use_cols = ['abs_file', 'video', 'group', 'label']

    # remove images with img_cond_feature < img_cond_value - by size or by goodface_prob
    img_df = project_utils.load_data(img_df_file)
    img_cond = img_df[img_cond_feature] <= img_cond_value 
    img_cond_len = len(img_cond[img_cond])
    img_cond = img_cond[~img_cond].reset_index(drop=True)
    print('remove %d outlier images' % img_cond_len)
    
    adf = None 
    if (sample_additional_train_size>0):
        adf = project_utils.load_data(additional_df_file)
        adf_true_idx = adf[adf['label']==1].index.values
        adf_fake_idx = adf[adf['label']==0].index.values
        adf_train_idx = np.hstack([adf_true_idx, adf_true_idx, adf_fake_idx, adf_fake_idx, adf_fake_idx, adf_fake_idx])
        adf_train_df = adf.loc[adf_train_idx].reset_index(drop=True)
        adf_true_fake_delta = len(adf_true_idx)*2 - len(adf_fake_idx)*4
        print(adf_train_df['label'].value_counts())        

    if use_baseline_valid:  #this branch won't be needed during reproduction runs
        baseline_meta = project_utils.load_data(baseline)
        if 'valid_list' in baseline_meta.keys():
            lval_list = baseline_meta['valid_list'].tolist()
            lval_df = img_df.loc[img_df['abs_file'].isin(lval_list)].reset_index(drop=True)
        if use_baseline_train:
            ltrain_list = baseline_meta['train_list'].tolist()
            ltrain_df = img_df.loc[img_df['abs_file'].isin(ltrain_list)].reset_index(drop=True)
        else:
            true_idx = img_df[img_df['label']==1].index.tolist()    
            fake_idx = img_df[img_df['label']==0].sample(len(true_idx)).index.tolist()
            img_df = img_df.loc[(true_idx+fake_idx)].reset_index(drop=True)                
            original_group_series = pd.Series(img_df[stratified_feature].unique())
            valid_original_groups = lval_df[stratified_feature].unique().tolist()
            train_original_groups = project_utils.list_difference(original_group_series.tolist(), valid_original_groups)
            train_idx = img_df.loc[~img_df[stratified_feature].isin(valid_original_groups)].index.values
            ltrain_df = img_df.iloc[train_idx].copy().reset_index(drop=True)
            
    else: #this branch will be used during reproduction runs
        true_idx = img_df[img_df['label']==1].index.tolist()    
        fake_idx = img_df[img_df['label']==0].sample(len(true_idx)).index.tolist()
        img_df = img_df.loc[(true_idx+fake_idx)].reset_index(drop=True)    
            
        if holdout_groups is None: # this branch won't be used during reproduction run
            print('ramdom sampling group with ratio %0.2f' % train_ratio)
            original_group_series = pd.Series(img_df[stratified_feature].unique())
            train_length = int(len(original_group_series) * train_ratio)
            train_original_groups = original_group_series.sample(train_length).tolist()
            valid_original_groups = project_utils.list_difference(original_group_series.tolist(), train_original_groups)
            val_idx = img_df.loc[img_df[stratified_feature].isin(valid_original_groups)].index.values
            train_idx = img_df.iloc[img_df.index.difference(val_idx)].index.values
        
        else:  #this branch will be used during reproduction runs  
            print('creating validaion set with holdout groups %s' % holdout_groups)
            valid_original_groups = holdout_groups            
            val_idx = img_df.loc[img_df[stratified_feature].isin(valid_original_groups)].index.values
            train_idx = img_df.iloc[img_df.index.difference(val_idx)].index.values

        ltrain_df = img_df.iloc[train_idx].copy().reset_index(drop=True)
        lval_df = img_df.iloc[val_idx].copy().reset_index(drop=True)
        
    if sample_train:
        if remove_fake_audio:
            print('removing images from fake audio')
            ltrain_df = ltrain_df.loc[~ltrain_df['video'].isin(fake_audios)].reset_index(drop=True)   
            
        if sample_additional_train_size>0:
            adf_delta = adf_true_fake_delta
        else:
            adf_delta = 0            
        ltrain_df = df_balanced_sampling(ltrain_df, sample_train_size, adf_delta=adf_delta)

    if (sample_additional_train_size>0) and (adf is not None):
#         sample_adf = df_balanced_sampling(adf, sample_additional_train_size, oversample=True)
#         sample_adf_filelist = sample_adf['abs_file'].tolist()
        ltrain_df = pd.concat([ltrain_df, adf_train_df], axis=0).reset_index(drop=True)
        print(adf_train_df.shape, ltrain_df.shape)
        
    if sample_valid:
        lval_df = df_balanced_sampling(lval_df, sample_valid_size)

        
    ltrain_vids_count = ltrain_df['original'].nunique()
    lval_vids_count = lval_df['original'].nunique()
    valid_original_groups = pd.Series(valid_original_groups).sort_values().tolist()

    print('train with %d images from %d original videos' % (len(ltrain_df), ltrain_vids_count))
    print('validate with %d images from %d original videos' % (len(lval_df), lval_vids_count))
    print('validation hold-out groups are %s' % valid_original_groups)
    print('train label value counts:')
    print(ltrain_df['label'].value_counts())
    print('valid label value counts:')
    print(lval_df['label'].value_counts())
    return ltrain_df[use_cols], lval_df[use_cols]





def get_data_transformers():
    transforms_train = albumentations.Compose([
        albumentations.Transpose(p=0.5),
        albumentations.VerticalFlip(p=0.5),
        albumentations.HorizontalFlip(p=0.5),
        albumentations.RandomBrightness(limit=0.2, p=0.75),
        albumentations.RandomContrast(limit=0.2, p=0.75),
        albumentations.OneOf([
            albumentations.MotionBlur(blur_limit=5),
            albumentations.MedianBlur(blur_limit=5),
            albumentations.GaussianBlur(blur_limit=5),
            albumentations.GaussNoise(var_limit=(5.0, 30.0)),
        ], p=0.5),

        albumentations.OneOf([
            albumentations.OpticalDistortion(distort_limit=1.0),
            albumentations.GridDistortion(num_steps=5, distort_limit=1.0),
            albumentations.ElasticTransform(alpha=3),
        ], p=0.7),

        albumentations.HueSaturationValue(hue_shift_limit=20, sat_shift_limit=20, val_shift_limit=20, p=0.5),
        albumentations.Cutout(max_h_size=cut_size, max_w_size=cut_size, num_holes=1, p=0.7),
    ])
    transforms_val = albumentations.Compose([
    ])
    return transforms_train, transforms_val






## main routine of model training script
if backup_script:
    try:
        src_file = os.path.basename(__file__)
        temp_backup_script = '../models/pytorch/' + gpu_id + '_' + time_str + '.py'
        shutil.copyfile(src_file, temp_backup_script)
    except:
        pass

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
record = {'train_loss': [], 'val_loss': [], 'val_acc': []}
ltrain_df, lval_df = get_dataframes()
transforms_train, transforms_val = get_data_transformers()

dataset_train = torch_utils.DeepFakeDataset(ltrain_df, 'train', 'train', image_size=image_size, mean_sub=mean_sub, transform=transforms_train)
dataset_valid = torch_utils.DeepFakeDataset(lval_df, 'train', 'val',   image_size=image_size, mean_sub=mean_sub, transform=transforms_val)

train_loader = torch.utils.data.DataLoader(dataset_train, batch_size=batch_size, sampler=RandomSampler(dataset_train), num_workers=num_workers)
valid_loader = torch.utils.data.DataLoader(dataset_valid, batch_size=batch_size, sampler=None, num_workers=num_workers)

model = torch_utils.enetv2(enet_type, out_dim=1)
model = model.to(device)
valid_loss_min = np.inf
model_file = f'{kernel_type}_best.pth'

print('Training All Layers...')
optimizer = opt_module(model.parameters(), lr=init_lr)
if use_apex:
    model, optimizer = amp.initialize(model, optimizer, opt_level="O1")    
scheduler_cosine = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, cosine_epo)
scheduler_warmup = GradualWarmupScheduler(optimizer, multiplier=10, total_epoch=warmup_epo, after_scheduler=scheduler_cosine)
for epoch in range(1, n_epochs+1):
    print(time.ctime(), 'Epoch:', epoch)
    scheduler_warmup.step(epoch-1)
    train_loss = torch_utils.train_epoch(model, train_loader, optimizer, device=device, use_amp=use_apex)
    print('')
    val_loss, acc = torch_utils.evaluate2(model, valid_loader, device=device)
    print('')
    content = time.ctime() + ' ' + f'Epoch {epoch}, lr: {optimizer.param_groups[0]["lr"]:.7f}, train loss: {np.mean(train_loss):.5f}, val loss: {np.mean(val_loss):.5f}, acc: {(acc):.5f}.'
    print(content)

    if val_loss < valid_loss_min:
        print('val_loss ({:.6f} --> {:.6f}).  Saving model ...'.format(valid_loss_min, val_loss))
        torch.save(model.state_dict(), model_file)
        valid_loss_min = val_loss

    record['train_loss'].append(np.mean(train_loss))
    record['val_loss'].append(val_loss)
    record['val_acc'].append(acc)

    
loss_str = 'val' + str(round(valid_loss_min,6))
model.load_state_dict(torch.load(model_file), strict=True)
save_file = kernel_type + '_' + loss_str + '.pth'
shutil.move(model_file, save_file)

eval_model = torch_utils.enetv2(enet_type, out_dim=1)
eval_model = eval_model.to(device)
eval_model.load_state_dict(torch.load(save_file), strict=True)
val_pred = torch_utils.evaluate2(eval_model, valid_loader, device=device, get_output=True)

try:
    dst_file = save_file[:-4] + '.py'
    shutil.move(temp_backup_script, dst_file)
except:
    pass



















